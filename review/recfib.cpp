#include <iostream>
using namespace std;

int fib(int);

int main(){
   for(int i = 0; i < 10; i++){
      cout << i << ": " << fib(i) << endl;
   }
}

int fib(int n){
   if(n == 0 || n == 1){
      return 1;
   }
   else{
      return fib(n - 2) + fib(n - 1);
   }
}
