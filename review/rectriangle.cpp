#include <iostream> 
using namespace std;

int blocks(int);

int main(){
   cout << "Enter int: ";
   int n;
   cin >> n;
   cout << blocks(n);
}

int blocks(int row){
   if(row == 1){
     return row;
   }
   else{
      return(row + blocks(row - 1));
   }
}
