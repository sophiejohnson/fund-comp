#include <iostream>
using namespace std;

void dispDown(int);

int main(){
   int n;
   cout << "Enter an integer." << endl;
   cin >> n;
   dispDown(n);
}

void dispDown(int n){
   if(n < 1){
      return;
   }
   else{
      cout << n << endl;
      dispDown(n - 1);
   }
}
