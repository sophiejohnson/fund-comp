// 9/26/17

#include <iostream>
#include <iomanip>
#include <cmath>
using namespace std;

int main(){

   // Size of data types in memory
   cout << "The size of int is " << sizeof(int) << endl;
   cout << "The size of float is " << sizeof(float) << endl;
   cout << "The size of double is " << sizeof(double) << endl;
   cout << "The size of char is " << sizeof(char) << endl;
   cout << "The size of bool is " << sizeof(bool) << endl;
   
   // Input/Output
   cout << "two integers please: ";
   int a, b;
   cin >> a >> b;
   cout << a << " to the " << b << " power is " << pow(a,b) << endl;
   
   // Operations truncate c = a (op) b to integer when 
   // 1) int a, int b (does not depend on c type)
   // 2) int c (does not depend on a or b type)
   float quotient;
   quotient =  a / b;
   cout << "a/b might be " << quotient << endl;
   cout << sizeof(quotient) << endl;
   cout << sizeof((double)quotient) << endl;

   // Declaration in while loop condition
   int i = 1;
   while (i <= 10){
      cout << setw(3) << right << i;
      i++;
   }

return 0;
}
