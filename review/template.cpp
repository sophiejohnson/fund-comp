#include <iostream>
using namespace std;

void func(void);
template <class T>
T func(T a, T b){
   return (a + b);
}

int main(){
   int a = 3, b = 5, c;
   c = func(a,b);
   cout << c;
   float d = 5.12, e = 3.35, f;
   f = func(d, e);
   cout << f;
   
   for (int i = 0; i < 7; i++){
      static char h = 'l';
      cout << h++;
   }
   
}

void func(void){
   static char h = 'l';
   h++;
   cout << h;
}
