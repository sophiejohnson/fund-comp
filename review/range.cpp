#include <iostream>
#include <vector>
#include <string>
using namespace std;

struct Num{
   int a;
   string name;
};

int main(){

   vector<vector<int>> a = {{3,6,2,7,3,3},
                            {6,9,0,3,1},
                            {4,6,3},
                            {4,8,2,1},
                            {3,8,8,9,9,3},
                            {3,3,5,6}};
   for(vector<int> i : a){
      for(auto it = i.begin(); it != i.end(); it++){
         cout << *it << " ";
      }
      cout << endl;
   }

   vector<string> at = {{"hello"},{"good"},{"bye"}};
   
   for(string b: at){
      cout << b << endl;
   }

   Num okay;
   okay.a = 9;
   okay.name = "nine";
   cout << okay.a << " " << okay.name << endl;


}
