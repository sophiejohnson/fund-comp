#include <iostream>
using namespace std;

int main(){

   char days[][20] = {"Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday","Does not exist"};

   cout << "Choose a day of the week (1 - 7): ";
   int choice;
   cin >> choice;
   if (choice > 0 && choice < 8){
      cout << days[choice - 1] << endl;
   }
   else
      cout << days[7] << endl;
      
   char today[][20] = {"interesting","I guess","sometimes"};
   cout << today[1] << endl;
   cout << today[1][0] << endl;
   char * p = &today[2][2];
   cout << p << endl;
}
