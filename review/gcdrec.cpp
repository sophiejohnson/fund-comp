#include <iostream>
using namespace std;

int gcd(int, int);

int main(){
   int a, b;
   cout << "Enter integers: ";
   cin >> a >> b;
   cout << "GCD: " << gcd(a, b) << endl;
}

int gcd(int q, int d){
   int r = q % d;
   if(r == 0){
      return d;
   }
   else{
      gcd(d, r);
   }
}
