#include <iostream>
using namespace std;

void func(int *);

int main(){
   
   // Initialize arrays of int and char
   int a[] = {3, 7, 8, 4, 4};
   int * p1 = a;
   char b[] = "hello";
   char * p2 = b;
   string s = "hi there";
   char * p3 = &s[0];
   
   // Display each value in int array
   for (int i = 0; i < 5; i++, p1++){
      cout << *p1 << endl;
   }
   p1 = a;
   // Add 2 to each value in int array
   for (int i = 0; i < 5; i++){
      *p1 += 2;
      cout << *p1 << endl;
      p1++;
   }
   
   // Verify that pointers pass by value
   cout << *p1 << endl;
   func(p1);
   cout << *p1 << endl;
   
   // Display char array/char address
   while (*p2){
      cout << *p2 << "    " << p2 << endl;
      p2++;
   }
   
   return 0;
}

void func(int * p){
   p++;
}
