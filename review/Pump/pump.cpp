#include <iostream>
#include <iomanip>
using namespace std;

#include "pump.h"

Pump::Pump(){
   setGallons(0);
   setPrice(0);
}

Pump::Pump(float g, float p){
   setGallons(g);
   setPrice(p);
}

Pump::~Pump(){
}

float Pump::getGallons(){
   return gallons;
}

float Pump::getPrice(){
   return price;
}

void Pump::setGallons(float g){
   gallons = g;
}

void Pump::setPrice(float p){
   price = p;
}

void Pump::showvalues(){
   cout << "The gas tank has " << gallons << " gallons of gas." << endl;
   cout << "the price per gallon of gas is $" << price << endl;
}

void Pump::request(float r){
   float pumped, remain, totPrice;
   remain = gallons - r;
   if (remain < 0){
      remain = 0;
      pumped = gallons;
   }
   else{
      pumped = r;
   }
   gallons = remain;
   totPrice = price * pumped;
   cout << r << " gallons were requested" << endl;
   cout << pumped << " gallons were pumped" << endl;
   cout << gallons << " gallons remain in the tank" << endl;
   cout << setprecision(2) << fixed;
   cout << "The total price is $" << totPrice << endl;
}
