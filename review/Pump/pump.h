class Pump{
   public:
      Pump();
      Pump(float, float);
      float getGallons();
      float getPrice();
      void setGallons(float);
      void setPrice(float);
      ~Pump();
      void showvalues();
      void request(float);
   private:
      float gallons;
      float price;
};
