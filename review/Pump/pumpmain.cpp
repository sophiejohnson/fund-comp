#include <iostream>
using namespace std;

#include "pump.h"

int main(){
   Pump p(200, 2.92);
   p.showvalues();
   cout << "* * *" << endl;
   p.request(60);
   cout << "* * *" << endl;
   p.request(175);
   return 0;
}
