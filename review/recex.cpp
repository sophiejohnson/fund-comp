#include <iostream>
#include <cmath>
using namespace std;

double recexp(double, int);
int fact(int);
double recexp2(double, double, int);

int main(){
   cout << recexp(2.542, 0) << endl;
   cout << recexp2(2.542, 1, 0) << endl;
}

double recexp(double x, int a){
   double term = pow(x, a) / fact(a);
   cout << term << endl;
   if(term <= 0.001){
      return term;
   }
   else{
      return term + recexp(x, a + 1);
   }
}

int fact(int n){
   int factorial = 1;
   for(n; n > 0; n--){
      factorial *= n;
   }
   return factorial;
}

double recexp2(double x, double term, int a){
   term = (a == 0)? 1 : term * (x / a);
   if(term < 0.001){
      return term;
   }
   else{
      return term + recexp2(x, term, a + 1);
   }
}
