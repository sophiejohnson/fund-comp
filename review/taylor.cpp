#include <iostream>
#include <cmath>
using namespace std;

double taylor(double);
double taylorrec(double, int);

int main(){
   double x = 0.432764;
   cout << taylor(x) << endl;
   cout << taylorrec(x, 0);
}

double taylor(double x){
   double sum = 0, term = 1;
   while(term > 0.001){
      sum += term;
      term *= x;
   }
   return sum;
}

double taylorrec(double x, int a){
   double term = pow(x, a);
   if(term <= 0.001){
      return term;
   }
   else{
      return(term + taylorrec(x, a + 1));
   }
}
