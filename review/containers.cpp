#include <iostream> 
#include <array>
#include <vector>
#include <fstream>

using namespace std;

template <class a>
a max (a &x, a &y){
   return (x > y)? x : y;
};

int main(){
   
   // Array and vector classes
   
   /* 
   array<int,5> arr{1,2,3,4,5};
   cout << arr.size();
   for (auto it = arr.begin(); it < arr.end(); it++){
      cout << *it << endl;
   }

   vector<char> vect {'n','o','p','e'};
   cout << vect.size();
   for (auto it = vect.begin(); it < vect.end(); it++){
      cout << *it << endl;
   }
   vect.push_back('o');
   cout << vect.size();
   */
   
   // Connecting to a file
   /*
   ifstream ifs;
   ifs.open("num.txt");
   if (!ifs){
      cout << "ERROR" << endl;
      return 1;
   }
   int a;
   ifs >> a;
   while (!ifs.eof()){
      cout << a << endl;
      ifs >> a;
   }
   */
   
   // Range based for loop
   
   /*
   vector<vector<int>> num{{1, 2, 3}, {3, 6}, {3, 5, 7}};
   for (vector<int> row: num){
      for (auto it = row.begin(); it != row.end(); it++){
         cout << *it << " ";
      }
      cout << endl;
   */
 
   // Using a template
   /*
   int a = 3, b = 4;
   float c = 6.6, d = 3.6;
   cout << max(a,b);
   cout << max(c,d);
   */
   
}
