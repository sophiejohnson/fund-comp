#include <iostream>
#include <vector>
using namespace std;

int countnum(vector<int>, int);
int countnum2(vector<int>, int);

int main(){
   vector<int> nums = {1, 2, 5, 4, 5, 6};
   int n = 5;
   cout << countnum(nums, n) << endl;
   cout << countnum2(nums, n) << endl;
}

int countnum(vector<int> v, int n){
   int freq = 0;
   for(auto it = v.begin(); it != v.end(); it++){
      if(*it == n){
         freq++;
      }
   }
   return freq;
}

int countnum2(vector<int> v, int n){
   int freq = 0;
   for(int i = 0; i < v.size(); i++){
      if(v[i] == n){
         freq++;
      }
   }
   return freq;
}
