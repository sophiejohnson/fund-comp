#include <iostream>
using namespace std;

#include "money.h"

Money::Money(){
   setMoney(0,0);
}

Money::Money(int d, int c){
   setMoney(d, c);
}

Money::~Money(){
}

int Money::getDollar(){
   return dollar;
}

int Money::getCent(){
   return cent;
}

void Money::setDollar(int d){
   dollar = d;
}

void Money::setCent(int c){
   cent = c;
   convert();
}

void Money::setMoney(int d, int c){
   dollar = d;
   cent = c;
   convert();
}
void Money::convert(){
   dollar += cent/100;
   cent = cent % 100;
   if (cent < 0){
      dollar -= 1;
      cent += 100;
   }
}

Money Money::operator+(Money a){
   return Money(a.dollar + dollar, a.cent + cent);
}

Money Money::operator-(Money a){
   return Money(dollar - a.dollar, cent - a.cent);
}

bool Money::operator>(Money a){
   bool isGreater;
   if (dollar > a.dollar){
      isGreater = true;
   }
   else if (dollar == a.dollar && cent > a.cent){
      isGreater = true;
   }
   else{
      isGreater = false;
   }
   return isGreater;
}

ostream & operator<< (ostream &s, Money &m){
   s << "$" << m.dollar << ".";
   if (m.cent < 10){
      s << "0" << m.cent;
   }
   else{
      s << m.cent;
   }
   return s;
}

istream & operator>> (istream &s, Money &m){
   int d, c;
   cout << "\tEnter the dollar value: ";
   s >> d;
   cout << "\tEnter the cents value: ";
   s >> c;
   m.setMoney(d,c);
   return s;
}
