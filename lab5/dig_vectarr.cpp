// Sophie Johnson
// CSE20311
// 10/2/17
// This program will find the frequency of occurence of digits in
// an array by using the <vector> class and iterating (for the digits
// array) and the <array> class and indexing (for the freq array).

#include <iostream>
#include <vector>
#include <array>
using namespace std;

void findfreq(vector<int> digits, array<int,10> &freq);
void results(array<int,10> &freq);

int main()
{
   // Array with random digits
   vector<int> digits = {4,3,6,5,7,8,9,4,6,3,1,3,5,7,6,3,6,
                         5,6,7,0,2,9,7,1,3,2,1,6,7,4,6,2,8,
                         1,4,5,6,0,2,7,6,4,5,6,8,3,5,7,1,5};
   // Array to tally frequency of digits
   array<int,10> freq = {};

   // Compute and display digits' frequencies
   findfreq(digits, freq);
   results(freq);

   return 0;
}

// Function to compute frequency of each digit
void findfreq(vector<int> digits, array<int,10> &freq)
{
   // Use iterator to point through digits vector
   for (auto it = digits.begin(); it < digits.end(); it++){
      freq[*it]++;
   }
}

// Function to display frequency of each digit
void results(array<int,10> &freq)
{
   // Use iterator it to point through frequency vector
   for (int i = 0; i < 10; i++){
      cout <<  "The digit " << i << " occurs " << freq[i] << " times." << endl;
   }
}
