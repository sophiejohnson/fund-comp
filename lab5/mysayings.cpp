// Sophie Johnson
// CSE 20311
// 10/7/17
// This program stores sayings. It begins by prompting the user for a
// startup data file. Then, it displays a menu of options for the user:
// display all sayings, add a new saying, list sayings containing a
// specific substring, save all sayings in a new text file, and quit
// the program.

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
using namespace std;

// Declare functions
void disp(vector<string>);
void disp2(vector<string>, string);

int main(){

   // Prompt user for startup data file
   cout << "Please enter a startup text file: ";
   string filename;
   cin >> filename;

   // Verify connection to data file
   ifstream ifs;
   ifs.open(filename);
   if (!ifs){
      cout << "ERROR: File could not be found." << endl;
      return 1;
   }

   // Read sayings from startup data file into vector
   vector<string> sayings;
   string newLine;
   getline(ifs, newLine);
   while (!ifs.eof()){
      sayings.push_back(newLine);
      getline(ifs, newLine);
   }

   // While loop until exit condition met
   bool doExit = false;
   while (!doExit){

      // Display options
      cout << endl;
      cout << "1) Display all sayings" << endl;
      cout << "2) Enter a new saying" << endl;
      cout << "3) List all sayings that contain a given word" << endl;
      cout << "4) Save all sayings in new text file" << endl;
      cout << "5) Quit the program" << endl;
      cout << "Please choose an option: ";

      // Check user input
      int userChoice = 0;
      cin >> userChoice;
      while (userChoice < 1 || userChoice > 5){
         cout << "Please enter an integer between 1 and 5: ";
         cin >> userChoice;
      }
      cout << endl;
      cin.ignore();

      // Perform action based on input
      switch (userChoice){

         // Display all sayings
		 case 1:{
		    disp(sayings);
	        break;
         }

         // Add new saying
		 case 2:{
		    // Prompt user for new saying
		    string userSaying;
		    cout << "Please enter a new saying: ";
		    getline(cin, userSaying);
		    sayings.push_back(userSaying);
		    break;
         }

         // List sayings containing given substring
		 case 3:{
            // Prompt user for substring
            cout << "Please enter a word or phrase: ";
            string phrase;
            getline(cin, phrase);
            // Display all strings containing substring
            disp2(sayings, phrase);
		    break;
		 }

         // Save all sayings in new text file
		 case 4:{
		    // Prompt user for name of new data file
		    cout << "Please enter output file name: ";
		    string filename2;
		    cin >> filename2;
		    // Verify output connection
		    ofstream ofs;
		    ofs.open(filename2);
		    if (!ofs){
		       cout << "ERROR: Could not create new file.";
		       return 2;
		    }
		    // Save sayings to new text file
			for (auto it = sayings.begin(); it < sayings.end(); it++){
      		   ofs << *it << endl;
      		}
		 break;
	     }

         // Quit program
		 case 5:{
		    doExit = true;
		 }
      } // End of switch
   } // End of while

   return 0;
} // End of main

// Function to display all sayings
void disp(vector<string> sayings){
   for (auto it = sayings.begin(); it < sayings.end(); it++){
      cout << *it << endl;
   }
}

// Function to display sayings with given phrase
void disp2(vector<string> sayings, string phrase){
   bool sayingFound = false;
   for (auto it = sayings.begin(); it < sayings.end(); it++){
      string line = *it;
      if (line.find(phrase) != string::npos){
         cout << *it << endl;
         sayingFound = true;
      }
   }
   if (!sayingFound){
      cout << "No sayings contain that phrase." << endl;
   }
}
