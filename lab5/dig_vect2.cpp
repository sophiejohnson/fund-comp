// Sophie Johnson
// CSE20311
// 10/2/17
// This program will find the frequency of occurence of digits in
// an array by using the <vector> class and iterators.

#include <iostream>
#include <vector>
using namespace std;

void findfreq(vector<int> digits, vector<int> &freq);
void results(vector<int> &freq);

int main()
{
   // Array with random digits
   vector<int> digits = {4,3,6,5,7,8,9,4,6,3,1,3,5,7,6,3,6,
                         5,6,7,0,2,9,7,1,3,2,1,6,7,4,6,2,8,
                         1,4,5,6,0,2,7,6,4,5,6,8,3,5,7,1,5};
   // Array to tally frequency of digits
   vector<int> freq = {0,0,0,0,0,0,0,0,0,0};

   // Compute and display digits' frequencies
   findfreq(digits, freq);
   results(freq);

   return 0;
}

// Function to compute frequency of each digit
void findfreq(vector<int> digits, vector<int> &freq)
{
   // Use iterator to increment frequency based on contents of digits vector
   for (auto it = digits.begin(); it < digits.end(); it++){
      freq[*it]++;
   }
}

// Function to display frequency of each digit
void results(vector<int> &freq)
{
   // Use iterator it to point through frequency vector
   for (auto it = freq.begin(); it < freq.end(); it++){
      cout <<  "The digit " << distance(freq.begin(), it) << " occurs " << *it << " times." << endl;
   }
}
