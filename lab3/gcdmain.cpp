// Sophie Johnson
// CSE20311
// 9/11/17
// Part 1: Simple Function
// This program will ask the user to input two integers and display the 
// greatest common divisor between them.

#include <iostream>
using namespace std;

// Declare function
int getgcd(int, int);

int main(){

   // Get input from user
   int num1;
   int num2;
   cout << "This program finds the greatest common denominator." << endl;
   cout << "Please enter two integers: ";
   cin >> num1;
   cin >> num2;

   // Call on getgcd function
   int gcd;
   gcd = getgcd(num1, num2);
   
   // Output results
   cout << "The greatest common divisor is " << gcd << "." << endl;

return 0;
}

// Function to find greatest common denominator
int getgcd(int num1, int num2){
   int gcd;
   // Consider cases when integers are 0
   if (num1 == 0){
      gcd = num2;
   }
   else if (num2 == 0){
      gcd = num1;
   }
   // Use algorithm to find gcd
   else{
      // Create boolean for while loop
      bool gcdFound = false;

      // Determine larger number
      int largeNum;
      int smallNum;

      if (num1 >= num2){
         largeNum = num1;
         smallNum = num2;         
      }
      else{
         largeNum = num2;
         smallNum = num1;
      }

      // Find gcd
      while (!gcdFound){
         // Divide to find remainder
         int result = largeNum / smallNum;
         int remainder = largeNum % smallNum;
         
         // Check to see if small number is the gcf
         if (remainder == 0){
            gcdFound = true;
            gcd = smallNum;
         }
 	 // If remainder isn't 0, then make smallNum the dividend
         // and remainder the divisor
         else{ 
            largeNum = smallNum;
            smallNum = remainder;
         }
      }
   }
   return gcd;
}
