// Sophie Johnson
// CSE 20311
// 9/11/17
// Part 2: Quadrants and Polar Coordinates
// This program prompts the user for the x and y coordinates of a point in a Cartesian
// coordinate system. It then finds and displays the corresponding polar coordinates
// and the quadrant which contains the point (or the axis it is on). 

#include <iostream>
#include <cmath>
#include <string>
using namespace std;

double getPolarRadius(double x, double y);
double getPolarAngle(double x, double y);
int getQuadrant(double x, double y);

int main(){

   // Prompt user for input
   cout << "Please enter the coordinates for a point (x,y) in the Cartesian coordinate system: ";
   double x;
   double y;
   cin >> x;
   cin >> y;

   // Find polar coordinates
   double r = getPolarRadius(x,y);
   double theta = getPolarAngle(x,y);
   int quadrant = 0;
   string location;

   // Find quadrant, axis, or origin
   if (x == 0 && y == 0){
      location = "on the origin.";
   }
   else if (x == 0){
      location = "on the y axis.";
   }
   else if (y == 0){
      location = "on the x axis.";
   }
   else{
      quadrant = getQuadrant(x,y);
      location = "in quadrant ";
   } 

   // Display results
   cout << "The point (" << x << "," << y << ") in polar coordinates is ";
   cout << "(" << r << "," << theta << ")." << endl;
   cout << "It is located " << location;
   if (quadrant != 0){
      cout << quadrant << ".";
   }
   cout << endl;

   return 0;
}

// Function to find polar radius
double getPolarRadius(double x, double y){
   double r = sqrt(pow(x,2) + pow(y,2));
return r;
}

// Function to find polar angle
double getPolarAngle(double x, double y){
   double theta = atan2(y,x);
return theta;
}

// Function to find quadrant
int getQuadrant(double x, double y){
   int quadrant;
   if ((x > 0) && (y > 0)){
   quadrant = 1;
   }
   else if ((x < 0) && (y > 0)){
   quadrant = 2;
   }
   else if ((x < 0) && (y < 0)){
   quadrant = 3;
   }
   else{
   quadrant = 4;
   }
return quadrant;
}
