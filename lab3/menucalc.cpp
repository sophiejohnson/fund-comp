// Sophie Johnson
// CSE20311
// 9/11/17
// Part 3: Text Menu Calculator
// This program allows the user to choose between calculator functions (addition,
// subtraction, multiplication, and division) and quit the program. After choosing
// an operation, the user inputs two values. The program displays the result.

#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

// Declare functions
double getSum(double, double);
double getDifference(double, double);
double getProduct(double, double);
double getQuotient(double, double);

int main(){

   // Greet user
   cout << endl;
   cout << "Hello! Welcome to the text menu calculator." << endl;
   cout << endl;

   // Initialize boolean for while loop
   bool quitProgram = false;

   while (!quitProgram){
      // Prompt user to choose operation
      cout << "1 = Addition" << endl;
      cout << "2 = Subtraction" << endl;
      cout << "3 = Multiplication" << endl;
      cout << "4 = Division" << endl;
      cout << "5 = Quit" << endl;
      cout << endl;
      cout << "Choose an action: ";
      
      // Get user input
      double userChoice;
      cin >> userChoice;

      // Check user input
      while ((userChoice != 1.) && (userChoice != 2.) && (userChoice != 3.) && (userChoice != 4.) && (userChoice != 5.)){
         cout << "ERROR     Enter an integer between 1 and 5: ";
         cin >> userChoice;
      }

      // Find result
      double result;      
      string operation;

      // Quit program if choice 5
      if (userChoice == 5.){
	 cout << "Thank you!" << endl;
         quitProgram = true;
      }

      else{   
         // Prompt user to enter two numbers
         cout << "Enter two numbers: ";
         double num1;
	 double num2;
	 cin >> num1;
         cin >> num2;
         cout << endl;

         // Call on appropriate function
         if (userChoice == 1.){ 
            result = getSum(num1, num2);
            operation = " + ";
         }
         else if (userChoice == 2.){
            result = getDifference(num1, num2);
	    operation = " - ";
         }
         else if (userChoice == 3.){       
            result = getProduct(num1, num2);
	    operation = " x ";
         }
         else if (userChoice == 4.){
            result = getQuotient(num1, num2);
	    operation = " / ";
         }
         
	 // Display results
         cout << fixed << setprecision(4);
         cout << num1 << operation << num2 << " = " << result << endl;
         cout << endl;
      }
   }        
   return 0;
}

// Addition function
double getSum(double a, double b){
   double sum = a + b;
   return sum;
}

// Subtraction function
double getDifference(double a, double b){
   double difference = a - b;
   return difference;
}

// Multiplication function
double getProduct(double a, double b){
   double product = a * b;
   return product;
}

// Division function
double getQuotient(double a, double b){
   double quotient = a / b;
   return quotient;
}
