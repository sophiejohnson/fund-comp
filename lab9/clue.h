// Sophie Johnson
// CSE 20311
// 11/18/17
// Interface for clue class

#include <string>

class Clue{
   friend ostream& operator<<(ostream&, Clue);
   public:
      Clue();
      Clue(int, int, string, string);
      int getRow();
      int getCol();
      string getOrientation();
      string getAnagram();
      void setRow(int);
      void setCol(int);
      void setOrientation(string);
      void setAnagram(string);
      void setClue(int, int, string, string);
   private:
      int row;
      int col;
      string orientation;
      string anagram;
};
