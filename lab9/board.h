// Sophie Johnson
// CSE 20311
// 11/14/17
// Interface for the board class

#include <vector>
#include <string>
#include "clue.h"

// Dimension of board (dim x dim)
const int dim = 15;

class Board{
   friend ostream& operator<<(ostream &, Board &);
   public:
      Board();
      ~Board();
      void makeCrossword(vector<string>);
      void order();
      void toUpper();
      void resetTemp();
      void updateBoard();
      void placeFirstWord();
      bool placeNextWord(int);
      void placeWords();
      bool checkVertical(int, int);
   private:
      char board[dim][dim];
      char temp[dim][dim];
      vector<string> words;
      vector<Clue> clues;
      bool allPlaced;
};

