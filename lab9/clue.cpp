// Sophie Johnson
// CSE 20311
// 11/18/17
// Implementation for clue class

#include <algorithm>
#include <iostream>
#include <iomanip>
using namespace std;

#include "clue.h"

Clue::Clue(){
}

Clue::Clue(int r, int c, string o, string a){
   setClue(r, c, o, a);
}

int Clue::getRow(){
   return row;
}

int Clue::getCol(){
   return col;
}

string Clue::getOrientation(){
   return orientation;
}

string Clue::getAnagram(){
   return anagram;
}

void Clue::setRow(int r){
   row = r;
}

void Clue::setCol(int c){
   col = c;
}

void Clue::setOrientation(string o){
   orientation = o;
}

void Clue::setAnagram(string a){
   anagram = a;
   random_shuffle(anagram.begin(), anagram.end());
}

void Clue::setClue(int r, int c, string o, string a){
   setRow(r);
   setCol(c);
   setOrientation(o);
   setAnagram(a);
}

// Overload output operator to display clue
ostream& operator<<(ostream& os, Clue c){
   os << setw(8) << c.col << "," << setw(3) << c.row;
   os << setw(16) << c.orientation << setw(16) << c.anagram;
}
