// Sophie Johnson
// CSE 20311
// 11/14/17
// This program uses a list of words from either user input or file input to
// generate an unsolved crossword puzzle, anagram clues for each word, and the
// solution. It then outputs to either the screen or a new file.

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
using namespace std;

#include "board.h"

void getUserWords(vector<string> &);
void getFileWords(vector<string> &, ifstream &);

int main(int argc, char *argv[]){

   vector<string> words;
   ifstream ifs;
   ofstream ofs;

   // If only executable, prompt user for input and store in vector
   if (argc == 1){
      getUserWords(words);
   }
   // If file name, read words from file into vector
   else if (argc == 2 || argc == 3){
      // Verify connection to file
      ifs.open(argv[1]);
      if (!ifs){
         cout << "ERROR: Could not connect to file." << endl;
         return 1;
      }
      getFileWords(words,ifs);
   }
   // If more than three arguments, error
   else{
      cout << "ERROR: Too many arguments." << endl;
      return 2;
   }

   // Create crossword with words
   Board crossword;
   crossword.makeCrossword(words);

   // Display solution, unsolved puzzle, and anagram clues to screen
   if ((argc == 1) || (argc == 2)){
      cout << crossword;
   }
   // If file name, output results to file
   else{
      // Verify connection to file
      ofs.open(argv[2]);
      if (!ofs){
         cout << "ERROR: Could not create new file." << endl;
         return 3;
      }
      ofs << crossword;
   }
   return 0;
}

// Store words from user into vector (max 20, '.' sentinel)
void getUserWords(vector<string> &words){
   string word;
   cout << "Please enter a list of words: ";
   cin >> word;
   while ((word != ".") && (words.size() <= 20)){
      words.push_back(word);
      cin >> word;
   }
}

// Store words from file into vector (max 20, '.' sentinel)
void getFileWords(vector<string> &words, ifstream &ifs){
   string word;
   ifs >> word;
   while ((word[0] != '.') && (words.size() <= 20)){
      words.push_back(word);
      ifs >> word;
   }
}
