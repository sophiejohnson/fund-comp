// Sophie Johnson
// CSE 20311
// 11/14/17

#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <cctype>
using namespace std;

#include "board.h"

// Constructor to initialize board and temporary board
Board::Board(){
   for(int x = 0; x < dim; x++){
      for(int y = 0; y < dim; y++){
         board[x][y] = '.';
         temp[x][y] = '.';
      }
   }
}

Board::~Board(){
}

// Method to make crossword
void Board::makeCrossword(vector<string> words){
   // Assign words to vector, convert to upper case, sort by length
   (this->words).swap(words);
   toUpper();
   order();
   // Place words on board
   placeWords();
}

// Order the words in vector from greatest to least
void Board::order(){
   vector<string> newWords;
   while (!words.empty()){
      // Find longest word in vector
      int maxLength = 0, maxWord = 0, i = 0;
      vector<string>::iterator it;
      for(it = words.begin(); it != words.end(); it++, i++){
         // Record position and length of longest word
         if((*it).length() > maxLength){
            maxLength = (*it).length();
            maxWord = i;
         }
      }
      // Store longest word in new vector
      newWords.push_back(*(words.begin() + maxWord));
      words.erase(words.begin() + maxWord);
   }
   // Update vector with sorted words
   words.swap(newWords);
}

// Convert all strings to uppercase
void Board::toUpper(){
   for(int i = 0; i < words.size(); i++){
      for(int j = 0; j < words[i].length(); j++){
         words[i][j] = toupper(words[i][j]);
      }
   }
}

// Set temporary board to match actual board
void Board::resetTemp(){
   for(int x = 0; x < dim; x++){
      for(int y = 0; y < dim; y++){
         temp[x][y] = board[x][y];
      }
   }
}

// Update actual board to match temporary board
void Board::updateBoard(){
   for(int x = 0; x < dim; x++){
      for(int y = 0; y < dim; y++){
         board[x][y] = temp[x][y];
      }
   }
}

// Place first word centered on board horizontally
void Board::placeFirstWord(){
   int i = 0, start = 7 - (words[0].length() / 2);
   Clue newClue;
   // Place each letter on board
   for(int y = start; y < start + words[0].length(); y++, i++){
      board[7][y] = words[0][i];
      temp[7][y] = words[0][i];
   }
   int row = 7, col = start;
   // Create clue for first word and add to vector
   newClue.setClue(7, start, "Across", words[0]);
   clues.push_back(newClue);
}

// Attempt to place word on board
bool Board::placeNextWord(int wordNum){
   bool playMade = false;
   int x = 0, y = 0, row = 0, col = 0;
   string orientation;
   Clue newClue;
   // Continue to check each space until word is placed
   while(!playMade && (x < dim)){
      y = 0;
      while(!playMade && (y < dim)){
         // Check each occupied space on board
         if(board[x][y] != '.'){
            // Check word in vector for matching character
            int wordLength = words[wordNum].length();
            for(int n = 0; n < wordLength; n++){
               if((board[x][y] == words[wordNum][n]) && !playMade){
                  playMade = true;
                  // Try to place word vertically
                  if(checkVertical(x, y)){
                     orientation = "Down";
                     // Check bounds
                     if((x - n < 0) || (x + wordLength - n > dim)){
                        playMade = false;
                     }
                     // If it fits on board, try to place each letter
                     else{
                        int k = 0;
                        row = x - n;
                        col = y;
                        for(int i = x - n; i < x + wordLength - n; i++){
                           // Skip root letter
                           if(i == x){
                              k++;
                           }
                           // For leftmost edge
                           else if((y == 0) && (board[i][y] == '.') && (board[i][y + 1] == '.')){
                              temp[i][y] = words[wordNum][k++];
                           }
                           // For rightmost edge
                           else if((y == dim - 1) && (board[i][y] == '.')  && (board[i][y - 1] == '.')){
                              temp[i][y] = words[wordNum][k++];
                           }
                           // For rest of board
                           else if((board[i][y] == '.') && (board[i][y - 1] == '.') && (board[i][y + 1] == '.')){
                              temp[i][y] = words[wordNum][k++];
                           }
                           // Otherwise, cannot be placed
                           else{
                              playMade = false;
                           }
                        }
                     }
                     // Check above first letter
                     if(x - n - 1 >= 0){
                        if(board[x - n - 1][y] != '.'){
                           playMade = false;
                        }
                     }
                     // Check below last letter
                     if(x + wordLength - n < dim){
                        if(board[x + wordLength - n][y] != '.'){
                           playMade = false;
                        }
                     }
                  }
                  // Try to place word horizontally
                  else{
                     orientation = "Across";
                     // Check bounds
                     if((y - n < 0) || (y + wordLength - n > dim)){
                        playMade = false;
                     }
                     // If it fits on board, try to place each letter
                     else{
                        int k = 0;
                        row = x;
                        col = y - n;
                        for(int j = y - n; j < y + wordLength - n; j++){
                           // Skip root letter
                           if(j == y){
                              k++;
                           }
                           // For top edge
                           else if((x == 0) && (board[x][j] == '.') && (board[x + 1][j] == '.')){
                              temp[x][j] = words[wordNum][k++];
                           }
                           // For bottom edge
                           else if((x == dim - 1) && (board[x][j] == '.') && (board[x - 1][j] == '.')){
                              temp[x][j] = words[wordNum][k++];
                           }
                           // For rest of board
                           else if((board[x][j] == '.') && (board[x - 1][j] == '.') && (board[x + 1][j] == '.')){
                              temp[x][j] = words[wordNum][k++];
                           }
                           // Otherwise, word cannot be placed
                           else{
                              playMade = false;
                           }
                        }
                     }
                     // Check next to leftmost letter
                     if(y - n - 1 >= 0){
                        if(board[x][y - n - 1] != '.'){
                           playMade = false;
                        }
                     }
                     // Check next to rightmost letter
                     if(y + wordLength - n < dim){
                        if(board[x][y + wordLength - n] != '.'){
                           playMade = false;
                        }
                     }
                  }
                  // If word was placed, update board and save clue for word
                  if(playMade){
                     updateBoard();
                     newClue.setClue(row, col, orientation, words[wordNum]);
                     clues.push_back(newClue);
                  }
                  // Otherwise reset temporary board
                  else{
                     resetTemp();
                  }
               }
            }
         }
         y++;
      }
      x++;
   }
   // Return boolean to indicate whether word was placed
   return playMade;
}

void Board::placeWords(){
   // Place first word on board
   placeFirstWord();
   // Place as many words on board as possible
   int wordNum = 1;
   bool playOn = true, noLonger = false;
   while((wordNum < words.size() && playOn)){
      playOn = placeNextWord(wordNum);
      if(wordNum != words.size() - 1){
         // If a word cannot be played, try all other words of same length
         if((!playOn) && (words[wordNum].length() == words[wordNum + 1].length())){
            playOn = true;
            noLonger = true;
         }
         // Once all words of minimum length have been tried, stop
         else if(noLonger && (words[wordNum].length() != words[wordNum + 1].length())){
            playOn = false;
         }
      }
      wordNum++;
   }
   // Record whether all words were placed on board
   if(playOn){
      allPlaced = true;
   }
   else{
      allPlaced = false;
   }
}

// Find whether word should be placed horizontally or vertically
bool Board::checkVertical(int x, int y){
   bool checkVert = false;
   // Check leftmost edge
   if((y == 0) && (board[x][y + 1] != '.')){
      checkVert = true;
   }
   // Check rightmost edge
   else if((y == dim - 1) && (board[x][y - 1]) != '.'){
      checkVert = true;
   }
   // Check rest of board
   else if((board[x][y - 1] != '.') || (board[x][y + 1] != '.')){
      checkVert = true;
   }
   return checkVert;
}

// Overload output operator to display solution, puzzle, and clues
ostream& operator<<(ostream &os, Board &crossword){
   // Output if all words could not be placed on puzzle
   if(!crossword.allPlaced){
      os << endl << "Note: Unable to place all entered words in crossword puzzle." << endl;
   }
   // Output solution
   os << endl << "SOLUTION:" << endl << endl;
   for(int x = 0; x < dim; x++){
      for (int y = 0; y < dim; y++){
         os << crossword.board[x][y] << " ";
      }
      os << endl;
   }
   os << endl;
   // Output unsolved puzzle
   os << "PUZZLE:" << endl << endl;
   for(int x = 0; x < dim; x++){
      for(int y = 0; y < dim; y++){
         if (crossword.board[x][y] != '.'){
            os << "  ";
         }
         else{
            os << "% ";
         }
      }
      os << endl;
   }
   os << endl;
   // Output clues
   os << "CLUES:" << endl << endl;
   os << setw(12) << "Coordinates:"; 
   os << setw(16) << "Orientation:" << setw(16) << "Anagram:" << endl;
   for(int i = 0; i < crossword.clues.size(); i++){
      os << crossword.clues[i] << endl;
   }
   return os;
}
