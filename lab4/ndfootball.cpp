// Sophie Johnson
// CSE20311
// 9/18/17
// Part	3: ND Football

// This program stores the win/loss record of Notre Dame football.
// It then prompts the user to choose from different options: display
// the record for a certain year, display the years with at least n wins,
// display the years with at least n losses, display all years with
// winning records, display all years with losing records, display all
// years in each set of n consecutive wins, display all years in each set of
// n consecutive losses, and exit.

#include <iostream>
using namespace std;

// Declare functions
int getWins(int year);
int getLosses(int year);
bool checkWinningRecord(int year);
bool checkLosingRecord(int year);

// Initialize arrays for wins and losses
int wins[] =
 { 6, 8, 6, 8, 5, 5, 6, 6, 8, 7, 4,
   6, 7, 7, 6, 7, 8, 6, 3, 9, 9, 10,
   8, 9, 10, 7, 9, 7, 5, 9, 10, 6, 6,
   3, 6, 7, 6, 6, 8, 7, 7, 8, 7, 9,
   8, 7, 8, 9, 9, 10, 4, 7, 7, 9, 9,
   8, 2, 7, 6, 5, 2, 5, 5, 2, 9, 7,
   9, 8, 7, 8, 10, 8, 8, 11, 10, 8, 9,
   11, 9, 7, 9, 5, 6, 7, 7, 5, 5, 8,
   12, 12, 9, 10, 10, 11, 6, 9, 8, 7, 9,
   5, 9, 5, 10, 5, 6, 9, 10, 3, 7, 6,
   8, 8, 12, 9, 8, 10, 4 };

int losses[] =
 { 3, 1, 2, 0, 3, 4, 1, 0, 1, 0, 1,
   0, 0, 0, 2, 1, 1, 1, 1, 0, 0, 1,
   1, 1, 0, 2, 1, 1, 4, 0, 0, 2, 2,
   5, 3, 1, 2, 2, 1, 2, 2, 0, 2, 1,
   2, 2, 0, 0, 0, 0, 4, 2, 2, 0, 1,
   2, 8, 3, 4, 5, 8, 5, 5, 7, 1, 2,
   0, 2, 2, 2, 1, 2, 3, 0, 2, 3, 3,
   1, 3, 4, 2, 6, 4, 5, 5, 6, 6, 4,
   0, 1, 3, 3, 1, 1, 5, 3, 3, 6, 3,
   7, 3, 6, 3, 7, 6, 3, 3, 9, 6, 6,
   5, 5, 1, 4, 5, 3, 8};

int main(){
   
   bool endProgram = false;
   bool yearFound = false;
   
   while (!endProgram){
   
   // Prompt user to choose action
   cout << "1: Displays record of specific year" << endl;
   cout << "2: Displays all years with winning record" << endl;
   cout << "3. Displays all years with losing record" << endl;
   cout << "4. Displays all years with at least n wins" << endl;
   cout << "5. Displays all years with at least n losses" << endl;
   cout << "6. Displays all years with n consecutive winning records" << endl;
   cout << "7. Displays all years with n consecutive losing records" << endl;
   cout << "8. Exit" << endl;
   cout << endl;
   cout << "Please choose an action: ";
   
   // Record user input
   int userChoice = 0;
   cin >> userChoice;

   // Check if input is valid integer, reprompt if necessary
   while ((userChoice < 1) || (userChoice > 8)){
      cout << "Please enter an integer between 1 and 8: ";
      cin >> userChoice;
   }

   // Initialize variables
   int numWins;
   int numLosses;
   int year;
   int numYearsDisplayed = 0;

   // Perform action based on choice
   switch (userChoice){
   
      // Find record for given year
      case 1:{
         // Record and check year entered by user
         cout << "Enter year between 1900 and present: ";
         cin >> year;
         while ((year < 1900) || (year > 1900 + (sizeof(wins) / sizeof(1)))){
            cout << "Please enter a year between 1900 and present: ";
            cin >> year;
         }
         // Find and display number of wins and losses
         numWins = getWins(year);
         numLosses = getLosses(year);
         cout << "The record in " << year << " was " << numWins << "-" << numLosses << "." << endl;
	     yearFound = true;
      }
         break;

      // Find years with a winning record
      case 2:{
         cout << "Years with a winning record: " << endl;
         // Check each year and output if winning record
         for (int i = 0; i < (sizeof(wins) / sizeof(1)); i++){
			year = 1900 + i;
            bool isWinningRecord = checkWinningRecord(year);
            if (isWinningRecord){
               cout << 1900 + i << " ";
               numYearsDisplayed++;
               // Format table
               if (numYearsDisplayed == 10){
                  cout << endl;
                  numYearsDisplayed = 0;
               }
               yearFound = true;
            }
         }
         if (yearFound){
            cout << endl;
      }
         }
         break;

      // Find years with a losing record
      case 3:{
         cout << "Years with a losing record: " << endl;
         // Check each year and output if losing record
         for (int i = 0; i < (sizeof(wins) / sizeof(1)); i++){
            year = 1900 + i;
            bool isLosingRecord = checkLosingRecord(year);
            if (isLosingRecord){
               cout << year << " ";
               numYearsDisplayed++;
               // Format table
               if (numYearsDisplayed == 10){
                  cout << endl;
                  numYearsDisplayed = 0;
               }
               yearFound = true;
            }
         }
         if (yearFound){
            cout << endl;
         }
      }
         break;

      // Find years with minimum number of wins
      case 4:{
         // Check and record number of wins entered by user
         cout << "Enter minimum number of wins: ";
         int minWins;
         cin >> minWins;
         while (minWins < 0){
            cout << "Please input a positive number of wins: ";
            cin >> minWins;
         }
         // Check and display each year with minimum number of wins
         cout << "Years with at least " << minWins << " wins: " << endl;
         for (int i = 0; i < (sizeof(wins) / sizeof(1)); i++){
            year = 1900 + i;
            numWins = getWins(year);
            if (numWins >= minWins){
               cout << year << " ";
               numYearsDisplayed++;
               // Format table
               if (numYearsDisplayed == 10){
                  cout << endl;
                  numYearsDisplayed = 0;
               }
               yearFound = true;
            }
         }
         if (yearFound){
            cout << endl;
         }
      }
         break;

      // Find years with minimum number of losses
      case 5:{
         // Check and record minimum number of losses entered by user
         cout << "Enter minimum number of losses: ";
         int minLosses;
         cin >> minLosses;
         while (minLosses < 0){
             cout << "Please enter a positive number of losses: ";
             cin >> minLosses;
         }
         // Find and display all years with minimum number of losses
         cout << "Years with at least " << minLosses << " losses: " << endl;
         for (int i = 0; i < (sizeof(losses) / sizeof(1)); i++){
            year = 1900 + i;
            numLosses = getLosses(year);
            if (numLosses >= minLosses){
               cout << year << " ";
               numYearsDisplayed++;
               // Format table
               if (numYearsDisplayed == 10){
                  cout << endl;
                  numYearsDisplayed = 0;
               }
               yearFound = true;
            }
         }
         if (yearFound){
            cout << endl;
         }
      }
         break;

 	  // Find years with number of consecutive winning seasons
      case 6:{
	     int consecutiveWins = 0;
	     int minConsecutiveWins;
	     // Check and record number of consecutive winning seasons entered by user
	     cout << "Enter minimum number of consecutive winning seasons: ";
	     cin >> minConsecutiveWins;
	     while (minConsecutiveWins < 0){
	        cout << "Please enter a positive number of consecutive winning seasons: ";
	        cin >> minConsecutiveWins;
	     }
	     // Find and display all years with minimum consecutive seasons with winning records
	     cout << "Years with at least " << minConsecutiveWins << " consecutive seasons with winning records:" << endl;
	     for (int i = 0; i < (sizeof(wins) / sizeof(1)); i++){
	        year = 1900 + i;
	        bool isWinningRecord = checkWinningRecord(year);
	        if (isWinningRecord){
	           consecutiveWins++;
	        }
	        else{
	           // Display range of years
	           if (consecutiveWins >= minConsecutiveWins){
	               cout << year - consecutiveWins << " to " << year - 1 << endl;
	               yearFound = true;
	           }
	           consecutiveWins = 0;
	        }
	        // Check for last element in list
	        if (isWinningRecord && (i == (sizeof(wins) / sizeof(1)) - 1)){
	           if (consecutiveWins >= minConsecutiveWins){
	              cout << year - consecutiveWins + 1 << " to " << year << endl;
	              yearFound = true;
	           }
	        }
	     }
      }
         break;
     // Find years with number of consecutive losing seasons
      case 7:{
          int consecutiveLosses = 0;
          int minConsecutiveLosses;
          // Check and record number of consecutive losing seasons entered by user
          cout << "Enter minimum number of consecutive seasons with losing records: ";
          cin >> minConsecutiveLosses;
          while (minConsecutiveLosses < 0){
              cout << "Please enter a positive number of consecutive losing seasons: ";
              cin >> minConsecutiveLosses;
          }
          // Find and display all years with minimum consecutive seasons with losing records
          cout << "Years with at least " << minConsecutiveLosses << " consecutive seasons with losing records:" << endl;
          for (int i = 0; i < (sizeof(losses) / sizeof(1)); i++){
             year = 1900 + i;
             bool isLosingRecord = checkLosingRecord(year);
             if (isLosingRecord){
                 consecutiveLosses++;
             }
             else{
                // Display range of years
                if (consecutiveLosses >= minConsecutiveLosses){
                   cout << year - consecutiveLosses << " to " << year - 1 << endl;
                   yearFound = true;
                }
                consecutiveLosses = 0;
             }
             // Check for last element in list
             if (isLosingRecord && (i == (sizeof(losses) / sizeof(1)) - 1)){
                if (consecutiveLosses >= minConsecutiveLosses){
                   cout << year - consecutiveLosses + 1 << " to " << year << endl;
                   yearFound = true;
                }
             }
          }
         break;
      }
      // Change boolean to end program
      case 8:{
         endProgram = true;
         yearFound = true;
      }   
         break;
   } // End of switch case
 
      // Output if no years met the condition
      if (!yearFound){
         cout << "No years meet this condition." << endl;
      }
      yearFound = false;
      cout << endl;

   } // End of while loop
return 0;
} // End of main function

// Function to find wins of specific year
int getWins(int year){
   int i = year - 1900;
   int numWins = wins[i];
   return numWins;
}

// Function to find losses of specific year
int getLosses(int year){
   int i = year - 1900;
   int numLosses = losses[i];
   return numLosses;
}

// Function to check for winning record
bool checkWinningRecord(int year){
   int i = year - 1900;
   bool isWinningRecord = false;
   if (wins[i] > losses[i]){
      isWinningRecord = true;
   }
   return isWinningRecord;
}

// Function to check for losing record
bool checkLosingRecord(int year){
   int i = year - 1900;
   bool isLosingRecord = false;
   if (wins[i] < losses[i]){
      isLosingRecord = true;
   }
   return isLosingRecord;
}

