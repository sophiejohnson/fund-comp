// Sophie Johnson
// CSE20311 
// 9/20/17
// Part 2: Prime Numbers
// This function will find all the prime numbers between 1 and 1000
// by using the sieve algorithm.

#include <iostream>
#include <iomanip>
using namespace std;

int main(){

   // Initialize array of 1000 ones
   int allNumbers [1001];
   for (int i = 0; i <= 1000; i++){
      allNumbers[i] = 1;
   }

   // Check each element that equals one (from 2 to 1000)
   for (int i = 2; i <= 1000; i++){
      if (allNumbers[i] == 1){
         // Set all elements which are its multiples to zero
         for (int k = i + 1; k <= 1000; k++){
            if ((k % i) == 0){
   	       allNumbers[k] = 0;
            }
         }
      }
   }

   // Display results
   int numOutput = 0;
   for (int i = 2; i <= 1000; i++){
      // Output number if element is prime (value of one)
      if (allNumbers[i] == 1){
         cout << setw(5) << right << i;
         numOutput++;
      }
      // Go to new row after ten numbers
      if (numOutput == 10){
         numOutput = 0;
	 cout << endl;
      }
   }
   cout << endl;

return 0;
}
