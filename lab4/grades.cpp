// Sophie Johnson
// CSE20311
// 9/18/17
// Part 1: A Simple Array and Input Redirection
// This program reads integer grade values from a data file into an array.
// Then, it computes the average and standard deviation.

#include <iostream>
#include <iomanip>
#include <cmath>
using namespace std;

int main(){

   // Initialize array, counter, and accumulation variable
   int grades[51];
   int i = 0;
   float sum = 0;

   // Read in numbers into an array and find sum (until input < 0)
   cin >> grades[i];
   while (grades[i] > 0){
      sum += grades[i];
      i++;
      cin >> grades [i];
   }

   // Find average of numbers in array
   float average = (sum / i);

   // Find standard deviation
   float squareSum = 0;
   float variance;
   float stdDeviation;
   for (int k = 0; k != i; k++){
      squareSum += pow((grades[k] - average) , 2);
   } 
   variance = squareSum / i;
   stdDeviation = sqrt(variance);

   // Display results 
   cout << fixed << setprecision(4);
   cout << "Average: " << average << endl;
   cout << "Standard Deviation: " << stdDeviation << endl;

return 0;
}
