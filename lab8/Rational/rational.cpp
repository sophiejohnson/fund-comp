// Sophie Johnson
// CSE 20311
// 10/26/17

#include <iostream>
using namespace std;

#include "rational.h"

Rational::Rational(){
   setRational(1, 1);
}

Rational::Rational(int n, int d){
   setRational(n,d);
}

Rational::~Rational(){
}

int Rational::getNumer(){
   return numer;
}

int Rational::getDenom(){
   return denom;
}

void Rational::setNumer(int n){
   numer = n;
   simplify();
}

void Rational::setDenom(int d){
   denom = d;
   simplify();
}

void Rational::setRational(int n, int d){
   numer = n;
   denom = d;
   simplify();
}

Rational Rational::operator+(Rational a){
   return Rational(numer * a.getDenom() + a.getNumer() * denom, a.getDenom() * denom);
}

Rational Rational::operator-(Rational a){
   return Rational(numer * a.getDenom() - a.getNumer() * denom, a.getDenom() * denom);
}

Rational Rational::operator*(Rational a){
   return Rational(numer * a.getNumer(), denom * a.getDenom());
}

Rational Rational::operator/(Rational a){
   return Rational(numer * a.getDenom(), denom * a.getNumer());
}

// Method to simplify rational number
void Rational::simplify(){
   // Determine whether numerator or denominator is larger
   if (numer != 0 && denom != 0){
      int n, d, r = 1;
      if (numer >= denom){
         n = numer;
         d = denom;
      }
      else{
         n = denom;
         d = numer;
      }
      // Use euclidean algorithm to find gcd
      while (r != 0){
         r = n % d;
         n = d;
         d = r;
      }
      // Divide by greatest common divisor
      numer /= n;
      denom /= n;
   }
}

// Overload the output operator
ostream& operator<< (ostream &s, Rational &r){
   // Display undefined if zero in denominator
   if (r.denom == 0){
      s << "undefined";
   }
   else if (r.numer == 0){
      s << 0;
   }
   // Display as integer if one in denominator
   else{
      // Adjust negative signs
      int scale = 1;
      if (r.denom < 0){
         scale = -1;
      }
      // Display integer if denominator is 1
      if ((r.denom == 1) || (r.denom == -1)){
         s << scale * r.numer;
      }
      // Otherwise display
      else{
         s << scale * r.numer << "/" << scale * r.denom;
      }
   }
}

// Overload the input operator
istream& operator>> (istream &s, Rational &r){
   double n, d;
   cout << "Enter the numerator and denominator: ";
   s >> n >> d;
   r.setRational(n, d);
}
