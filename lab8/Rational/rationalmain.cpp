// Sophie Johnson
// CSE 20311
// 10/23/17
// This program tests the rational class.

#include <iostream>
#include "rational.h"
using namespace std;

int main(){
   
   // Prompt user to enter rational number
   cout << "\nEnter a rational number. " << endl;
   Rational a;
   cin >> a;
   
   // Display rational number
   cout << "\tA = " << a << endl;
   
   // Output either the numerator or denominator
   cout << "Choose 1) numerator or 2) denominator: ";
   int userChoice;
   cin >> userChoice;
   while (userChoice != 1 && userChoice != 2){
      cout << "Please enter 1 or 2: ";
      cin >> userChoice;
   }
   if (userChoice == 1){
      cout << "\tNumerator: " << a.getNumer() << endl;
   }
   else if (userChoice == 2){
      cout << "\tDenominator: " << a.getDenom() << endl;
   }
   
   // Create and display default rational number
   Rational b = Rational();
   cout << "\nDefault rational number: \n\tB = " << b << endl;

   // Change rational number numerator
   cout << "Please enter a new numerator for the default number: ";
   int x;
   cin >> x;
   b.setNumer(x);
   cout << "\tB = " << b << endl;
   
   // Change rational number denominator
   cout << "Please enter a new denominator for the default number: ";
   cin >> x;
   b.setDenom(x);
   cout << "\tB = " << b << endl;

   // Test all operations
   Rational sum = a + b;
   Rational difference = a - b;
   Rational product = a * b;
   Rational quotient = a / b;
   
   // Display all results
   
   cout << "A + B = " << sum << endl;
   cout << "A - B = " << difference << endl;
   cout << "A x B = " << product << endl;
   cout << "A / B = " << quotient << endl;

   // Check display of signs
   Rational c(-1,1);
   Rational a1 = a * c;
   cout << "\nIf C = " << c << ", A x C = " << a1 << "." << endl;
   
   // Check zero in denominator
   a.setDenom(0);
   cout << "If the denominator of A = 0, A = " << a << "." << endl;

   return 0;
}
