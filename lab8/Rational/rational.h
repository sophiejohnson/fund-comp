// Sophie Johnson
// CSE 20311
// 10/26/17
// rational.h

using namespace std;

class Rational {
   friend ostream& operator<< (ostream &, Rational &);
   friend istream& operator>> (istream &, Rational &);
   public:
      Rational();
      Rational(int, int);
      ~Rational();
      int getNumer();
      int getDenom();
      void setNumer(int);
      void setDenom(int);
      void setRational(int,int);
      Rational operator+(Rational);
      Rational operator-(Rational);
      Rational operator*(Rational);
      Rational operator/(Rational);
      void simplify();
   private:
      int numer;
      int denom;
};
