// Sophie Johnson
// CSE 20311
// 11/5/17

#include <iostream>
#include <unistd.h>
#include <stdlib.h>
#include <fstream>

#include "lifeboard.h"
using namespace std;

void dispOptions();
void playGame(lifeboard &);

main(int argc, char *argv[]){
   lifeboard mainBoard;
   
   // If no command line arguments, enter interactive mode
   if (argc == 1){
      bool doExit = false;
      while (!doExit){
         // Prompt user to choose action
         system("clear");
         mainBoard.print();
         dispOptions();
         char userChoice;
         cin >> userChoice;
         // Perform appropriate action
         switch (userChoice){
            // Add new live cell
            case 'a':
               mainBoard.userAddLiveCell();
               break;
            // Remove a live cell
            case 'r':
               mainBoard.userRemoveLiveCell();
               break;
            // Perform next iteration
            case 'n':
               mainBoard.updateBoard();
               break;
            // Quit program
            case 'q':
               doExit = true;
               break;
            // Play game continuously
            case 'p':
               playGame(mainBoard);
               break;
            // Error message if command not recognized
            default:
               cout << "ERROR: Command not recognized." << endl;
         }
      }
   }
   
   // If data file entered after executable, enter batch mode
   else if (argc == 2){
      // Connect to data file and verify connection
      ifstream ifs;
      ifs.open(argv[1]);
      if (!ifs){
         cout << "ERROR: Unable to connect to file." << endl;
         return 1;
      }
      char choice;
      ifs.get(choice);
      // Perform appropriate action
      switch (choice){
         // Add new live cell
         case 'a':
            ifs.get(x);
            ifs.get(y);
            mainBoard.addLiveCell(x, y);
         break;
         // Remove a live cell
         case 'r':
            ifs.get(x);
            ifs.get(y);
            mainBoard.removeLiveCell(x, y);
            break;
         // Perform next iteration
         case 'n':
            mainBoard.updateBoard();
            break;
         // Quit program
            case 'q':
            doExit = true;
            break;
         // Play game continuously
            case 'p':
            playGame(mainBoard);
            break;
      }
   }
   
   // If too many command line arguments, output error message
   else{
      cout << "Too many arguments. Please try again." << endl;
   }
   return 0;
}

// Function to display options
void dispOptions(){
   cout << "a) Add a new live cell" << endl;
   cout << "r) Remove a live cell" << endl;
   cout << "n) Advance to next iteration of the game" << endl;
   cout << "q) Quit the program" << endl;
   cout << "p) Play the game continuously" << endl;
   cout << "Please choose an option: ";
}

// Function to play game continuously
void playGame(lifeboard &mainBoard){
   while (true){
      // After displaying board for one second, clear screen
      usleep(500000);
      system("clear");
      // Update board and display
      mainBoard.updateBoard();
      mainBoard.print();
   }
}
