// Sophie Johnson
// CSE 20311
// 11/5/17

#include <iostream>
#include <unistd.h>
#include <stdlib.h>
#include <fstream>

#include "lifeboard.h"
using namespace std;

void dispOptions();
void playGame(lifeboard &);

main(int argc, char *argv[]){
   lifeboard mainBoard;
   // If data file entered after executable, connect to file
   if (argc == 2){
      ifstream ifs;
      ifs.open(argv[1]);
      if (!ifs){
         cout << "ERROR: Unable to connect to file." << endl;
         return 1;
      }
   }
   // If too many command line arguments, output error message
   if (argc > 2){
      cout << "Too many arguments. Please try again." << endl;
   }
   // Enter interactive/batch mode
   else{
      bool doExit = false;
      bool interactive = false;
      while (!doExit){
         // Display board
         system("clear");
         mainBoard.print();
         // If no command line arguments, prompt user
         char choice;
         if (argc == 1){
            dispOptions();
            cin >> choice;
            bool interactive = true;
         }
         // If connected to file, get input from file
         else{
            ifs.get(choice);
         }
         // Perform appropriate action
         switch (choice){
            // Add new live cell
            case 'a':
               if (interactive){
                  mainBoard.userAddLiveCell();
               }
               else{
                  mainBoard.fileAddLiveCell(ifs);
               }
            break;
            // Remove a live cell
            case 'r':
               if (interactive){
                  mainBoard.userRemoveLiveCell();
               }
               else{
                  mainBoard.fileAddLiveCell(ifs);
               }
            break;
            // Perform next iteration
            case 'n':
               mainBoard.updateBoard();
            break;
            // Quit program
            case 'q':
               doExit = true;
            break;
            // Play game continuously
            case 'p':
               playGame(mainBoard);
            break;
            // Error message if command not recognized
            default:
               cout << "ERROR: Command not recognized." << endl;
         }
      }
   }
   return 0;
}

// Function to display options
void dispOptions(){
   cout << "a) Add a new live cell (x, y)" << endl;
   cout << "r) Remove a live cell (x, y)" << endl;
   cout << "n) Advance to next iteration of the game" << endl;
   cout << "q) Quit the program" << endl;
   cout << "p) Play the game continuously" << endl;
   cout << "Please choose an option: ";
}

// Function to play game continuously
void playGame(lifeboard &mainBoard){
   while (true){
      // After displaying board for one second, clear screen
      usleep(500000);
      system("clear");
      // Update board and display
      mainBoard.updateBoard();
      mainBoard.print();
   }
}
