// Sophie Johnson
// CSE 20311
// 11/5/17
// Interface for the lifeboard class

#include <fstream>
using namespace std;

// Dimension for lifeboard (dim x dim)
const int dim = 40;

class lifeboard {
   public:
      lifeboard();
      ~lifeboard();
      char getValue(int, int);
      void userAddLiveCell();
      void userRemoveLiveCell();
      void fileAddLiveCell(ifstream &);
      void fileRemoveLiveCell(ifstream &);
      void addLiveCell(int, int);
      void removeLiveCell(int, int);
      void updateBoard();
      void updateSpace(int, int);
      void swapBoard();
      void print();
   private:
      char board[dim][dim];
      char tempBoard[dim][dim];
};

