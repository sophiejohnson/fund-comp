// Sophie Johnson
// CSE 20311
// 11/5/17
// Implementation for the lifeboard class

#include <iostream>
#include <fstream>
#include "lifeboard.h"
using namespace std;

lifeboard::lifeboard(){
   for(int x = 0; x < dim; x++){
      for (int y = 0; y < dim; y++){
         board[x][y] = ' ';
      }
   }
}

lifeboard::~lifeboard(){

}

char lifeboard::getValue(int x, int y){
   return(board[x][y]);
}

// Method for user to add a live cell
void lifeboard::userAddLiveCell(){
   int x, y;
   cin >> x >> y;
   while ((x < 0) || (x > dim - 1)){
      cout << "Please enter an x value between 0 and " << dim - 1<< ": ";
      cin >> x;
   }
   while ((y < 0) || (y > dim - 1)){
      cout << "Please enter a y value between 0 and " << dim - 1<< ": ";
      cin >> y;
   }
   addLiveCell(x, y);
}

// Method for user to remove a live cell
void lifeboard::userRemoveLiveCell(){
   int x, y;
   cin >> x >> y;
   while ((x < 0) || (x > dim - 1)){
      cout << "Please enter an x value between 0 and " << dim - 1 << ": ";
      cin >> x;
   }
   while ((y < 0) || (y > dim - 1)){
      cout << "Please enter a y value between 0 and " << dim - 1 << ": ";
      cin >> y;
   }
   removeLiveCell(x, y);
}

// Method for data file to add a live cell
void lifeboard::fileAddLiveCell(ifstream &ifs){
   int x, y;
   ifs >> x >> y;
   if ((x >= 0) && (x <= dim - 1) && (y >= 0) && (y <= dim - 1)){
      addLiveCell(x, y);
   }
}

// Method for data file to remove a live cell
void lifeboard::fileRemoveLiveCell(ifstream &ifs){
   int x, y;
   ifs >> x >> y;
   if ((x >= 0) && (x <= dim - 1) && (y >= 0) && (y <= dim - 1)){
      removeLiveCell(x, y);
   }
}

// Method to add a live cell
void lifeboard::addLiveCell(int x, int y){
   board[x][y] = 'X';
}

// Method to remove a live cell
void lifeboard::removeLiveCell(int x, int y){
   board[x][y] = ' ';
}

// Method to update each space of board
void lifeboard::updateBoard(){
   for(int x = 0; x < dim; x++){
      for (int y = 0; y < dim; y++){
         // Update each space individually
         updateSpace(x, y);
      }
   }
   // Set lifeboard to temporary board
   swapBoard();
}

// Method to update an individual space
void lifeboard::updateSpace(int xCoord, int yCoord){
   int numNeighbors = 0;
   // Check block of nine spaces
   for (int x = xCoord - 1; x <= xCoord + 1; x++){
      for (int y = yCoord - 1; y <= yCoord + 1; y++){
         // Skip space being evaluated
         if ((x == xCoord) && (y == yCoord)){
         }
         // Count neighbors (excluding spaces outside of board)
         else if ((x >= 0) && (x < dim) && (y >= 0) && (y < dim)){
            if (board[x][y] == 'X'){
               numNeighbors++;
            }
         }
      }
   }
   // If three neighbors, cell is alive
   if (numNeighbors == 3){
      tempBoard[xCoord][yCoord] = 'X';
   }
   // If two neighbors and cell is alive, stays alive
   else if ((board[xCoord][yCoord] == 'X') && (numNeighbors == 2)){
      tempBoard[xCoord][yCoord] = 'X';
   }
   // Otherwise, cell is dead
   else{
      tempBoard[xCoord][yCoord] = ' ';
   }
}

// Method to set lifeboard to temporary board
void lifeboard::swapBoard(){
   for(int x = 0; x < dim; x++){
      for (int y = 0; y < dim; y++){
         board[x][y] = tempBoard[x][y];
      }
   }
}

// Method to display the board
void lifeboard::print(){
   for(int x = 0; x < dim; x++){
      cout << "* ";
      for (int y = 0; y < dim; y++){
         cout << board[x][y] << " ";
      }
      cout << "* " << endl;
   }
}
