// Sophie Johnson
// CSE 20311
// 11/5/17
// Implementation for the lifeboard class

#include <iostream>
#include "lifeboard.h"
using namespace std;

lifeboard::lifeboard(){
   for(int i = 0; i < dim; i++){
      for (int j = 0; j < dim; j++){
         board[i][j] = ' ';
      }
   }
}

lifeboard::~lifeboard(){

}

char lifeboard::getValue(int x, int y){
   return(board[x - 1][y - 1]);
}

// Method for user to add a live cell
void lifeboard::userAddLiveCell(){
   int x, y;
   cin >> x >> y;
   while ((x < 1) || (x > dim)){
      cout << "Please enter an x value between 1 and " << dim << ": ";
      cin >> x;
   }
   while ((y < 1) || (y > dim)){
      cout << "Please enter a y value between 1 and " << dim << ": ";
      cin >> y;
   }
   addLiveCell(x, y);
}

// Method for user to remove a live cell
void lifeboard::userRemoveLiveCell(){
   int x, y;
   cin >> x >> y;
   while ((x < 1) || (x > dim)){
      cout << "Please enter an x value between 1 and " << dim << ": ";
      cin >> x;
   }
   while ((y < 1) || (y > dim)){
      cout << "Please enter a y value between 1 and " << dim << ": ";
      cin >> y;
   }
   removeLiveCell(x, y);
}

void lifeboard::addLiveCell(int x, int y){
   board[x - 1][y - 1] = 'X';
}

void lifeboard::removeLiveCell(int x, int y){
   board[x - 1][y - 1] = ' ';
}

// Method to update each space of board
void lifeboard::updateBoard(){
   for(int i = 0; i < dim; i++){
      for (int j = 0; j < dim; j++){
         updateSpace(i, j);
      }
   }
   swapBoard();
}

// Method to update an individual space
void lifeboard::updateSpace(int iCoord, int jCoord){
   int numNeighbors = 0;
   // Check block of nine spaces
   for (int i = iCoord - 1; i <= iCoord + 1; i++){
      for (int j = jCoord - 1; j <= jCoord + 1; j++){
         // Skip space being evaluated
         if ((i == iCoord) && (j == jCoord)){
         }
         // Count neighbors (excluding spaces outside of board)
         else if ((i >= 0) && (i < dim) && (j >= 0) && (j < dim)){
            if (board[i][j] == 'X'){
               numNeighbors++;
            }
         }
      }
   }
   // If three neighbors, cell is alive
   if (numNeighbors == 3){
      tempBoard[iCoord][jCoord] = 'X';
   }
   // If two neighbors and cell is alive, stays alive
   else if ((board[iCoord][jCoord] == 'X') && (numNeighbors == 2)){
      tempBoard[iCoord][jCoord] = 'X';
   }
   // Otherwise, cell is dead
   else{
      tempBoard[iCoord][jCoord] = ' ';
   }
}

// Method to swap boards
void lifeboard::swapBoard(){
   for(int i = 0; i < dim; i++){
      for (int j = 0; j < dim; j++){
         board[i][j] = tempBoard[i][j];
      }
   }
}

// Method to display the board
void lifeboard::print(){
   for(int i = 0; i < dim; i++){
      cout << "* ";
      for (int j = 0; j < dim; j++){
         cout << board[i][j] << " ";
      }
      cout << "* " << endl;
   }
}
