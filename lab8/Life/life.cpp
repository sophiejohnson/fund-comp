// Sophie Johnson
// CSE 20311
// 11/5/17
// This program utilizes the lifeboard class to display an animation of
// Conway's game of life.

#include <iostream>
#include <unistd.h>
#include <stdlib.h>
#include <fstream>
#include "lifeboard.h"
using namespace std;

void dispOptions();
void playGame(lifeboard &);

main(int argc, char *argv[]){
   // Initialize board
   lifeboard mainBoard;
   // If too many command line arguments, output error message
   if (argc > 2){
      cout << "ERROR: Too many arguments." << endl;
      return 2;
   }
   // Otherwise, enter interactive/batch mode
   else{
      bool interactive = true;
      // If data file entered after executable (batch mode), connect to file
      ifstream ifs;
      if (argc == 2){
         ifs.open(argv[1]);
         // Verify connection
         if (!ifs){
            cout << "ERROR: Unable to connect to file." << endl;
            return 1;
         }
         // Boolean determines whether interactive or batch mode
         interactive = false;
      }
      bool doExit = false;
      while (!doExit){
         // Display board
         system("clear");
         mainBoard.print();
         // If no command line arguments (interactive mode), prompt user for input
         char choice;
         if (argc == 1){
            dispOptions();
            cin >> choice;
         }
         // If connected to file (batch mode), get input from file
         else{
            ifs.get(choice);
         }
         // Perform appropriate action
         switch (choice){
            // Add new live cell (either from user input or file)
            case 'a':
               if (interactive){
                  mainBoard.userAddLiveCell();
               }
               else{
                  mainBoard.fileAddLiveCell(ifs);
               }
            break;
            // Remove a live cell (either from user input or file)
            case 'r':
               if (interactive){
                  mainBoard.userRemoveLiveCell();
               }
               else{
                  mainBoard.fileRemoveLiveCell(ifs);
               }
            break;
            // Perform next iteration
            case 'n':
               mainBoard.updateBoard();
            break;
            // Quit program
            case 'q':
               doExit = true;
            break;
            // Play game continuously
            case 'p':
               playGame(mainBoard);
            break;
         }
      }
   }
   return 0;
}

// Function to display options
void dispOptions(){
   cout << "a) Add a new live cell (x, y)" << endl;
   cout << "r) Remove a live cell (x, y)" << endl;
   cout << "n) Advance to next iteration of the game" << endl;
   cout << "q) Quit the program" << endl;
   cout << "p) Play the game continuously" << endl;
   cout << "Please choose an option: ";
}

// Function to play game continuously
void playGame(lifeboard &mainBoard){
   while (true){
      // After displaying board and pausing, clear screen
      usleep(100000);
      system("clear");
      // Update board and display
      mainBoard.updateBoard();
      mainBoard.print();
   }
}
