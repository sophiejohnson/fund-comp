// Sophie Johnson
// CSE 20311
// 12/4/17

#include <cmath>
#include "gfx.h"
using namespace std;

void sierpinski(int, int, int, int, int, int);
void shrinkingSquares(int, int, int, int, int, int, int, int);
void spiralSquares(int, int, int, int, float);
void circularLace(int, int, int);
void snowflake(int, int, int);
void tree(float, float, float, float);
void fern(float, float, float, float);
void spiralOfSpirals(float, float, float, float);
void drawTriangle(int, int, int, int, int, int);
void drawSquare(int, int, int, int, int, int, int, int);

const float pi = 3.14159265;

int main(){
   // Set parameters and open window
   int wid = 500, ht = 500, marg = 10;
   char c = 0;
   gfx_open(wid, ht, "Fractals");
   // Respond to user input
   while(c != 'q'){
      c = gfx_wait();
      // Clear screen
      gfx_clear();
      switch (c){
         // Create sierpinski triangle
         case '1':
            sierpinski(marg, marg, wid - marg, marg, wid / 2, ht - marg);
            break;
         // Create shrinking squares
         case '2':{
            float a = .25, b = .75;
            shrinkingSquares(wid * a, ht * a, wid * b, ht * a, wid * b, ht * b, wid * a, ht * b);
            break;
            }
         // Create spiral squares
         case '3':
            spiralSquares(wid / 2, ht / 2, 1, 1, 0);
            break;
         // Create circular lace
         case '4':
            circularLace(wid / 2, ht / 2, 175);
            break;
         // Create snowflake
         case '5':
         	snowflake(wid / 2, ht / 2, 160);
        	break;
         // Create tree
         case '6':
            tree(wid / 2, ht - marg, wid / 2, 325);
            break;
         // Create fern
         case '7':
            fern(wid / 2, ht - marg, wid / 2, 175);
            break;
         // Create spiral of spirals
         case '8':
            spiralOfSpirals(wid / 2, ht / 2, 400, 0);
            break;
      }
   }
}

// Create sierpinski triangle
void sierpinski(int x1, int y1, int x2, int y2, int x3, int y3){
   // Base case for length of triangle side
   if(abs(x2 - x1) < 5){
      return;
   }
   // Draw triangle
   drawTriangle(x1, y1, x2, y2, x3, y3);
   // Recursive step to draw three triangles inside
   sierpinski(x1, y1, (x1 + x2) / 2, (y1 + y2) / 2, (x1 + x3) / 2, (y1 + y3) / 2);
   sierpinski((x1 + x2) / 2, (y1 + y2) /2, x2, y2, (x2 + x3) / 2, (y2 + y3) / 2);
   sierpinski((x1 + x3) / 2, (y1 + y3) / 2, (x2 + x3) / 2, (y2 + y3) / 2, x3, y3);
}

// Create shrinking squares
void shrinkingSquares(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4){
   // Base case for length of square side
   int len = abs(x2 - x1);
   if(len < 1){
      return;
   }
   // Draw square
   drawSquare(x1, y1, x2, y2, x3, y3, x4, y4);
   // Recursive step to draw four smaller squares centered at each corner
   int a = len * .23;
   shrinkingSquares(x1 - a, y1 - a, x1 + a, y2 - a, x1 + a, y1 + a, x1 - a, y1 + a);
   shrinkingSquares(x2 - a, y2 - a, x2 + a, y2 - a, x2 + a, y2 + a, x2 - a, y2 + a);
   shrinkingSquares(x3 - a, y3 - a, x3 + a, y3 - a, x3 + a, y3 + a, x3 - a, y3 + a);
   shrinkingSquares(x4 - a, y4 - a, x4 + a, y4 - a, x4 + a, y4 + a, x4 - a, y4 + a);
}

// Create spiral of squares
void spiralSquares(int xc, int yc, int a, int r, float angle){
   // Base case for radius of spiral
   if(r > 300){
      return;
   }
   // Draw square
   int x = xc + r * cos(angle);
   int y = yc - r * sin(angle);
   drawSquare(x - a, y - a, x + a, y - a, x + a, y + a, x - a, y + a);
   // Recursive step to draw larger square at next position
   spiralSquares(xc, yc, a + 1, r + 10, angle + pi / 6);
}

// Create circular lace
void circularLace(int x, int y, int r){
   // Base case for radius of circle
   if(r < 1){
      return;
   }
   // Draw circle
   gfx_circle(x, y, r);
   // Recursive step to add six circles around circle circumference
   float angle = pi / 3;
   for(int i = 0; i < 6; i++){
      circularLace(x + r * cos(angle * i), y - r * sin(angle * i), r * 0.34);
   }
}

// Create snowflake
void snowflake(int x, int y, int len){
   // Base case for length of line segment
   if(len < 2){
      return;
   }
   // Draw lines and recurse to draw five lines from end of each line segment
   float angle = 2 * pi / 5;
   for(int i = 0; i < 5; i++){
      gfx_line(x, y, x + len * cos(angle * i), y - len * sin(angle * i));
      snowflake(x + len * cos(angle * i), y - len * sin(angle * i), len * 0.38);
   }
}

// Create tree
void tree(float x1, float y1, float x2, float y2){
   // Base case for length of line segment
   float len = sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2));
   if(len < 2){
      return;
   }
   // Draw line segment
   gfx_line(x1, y1, x2, y2);
   // Recursive step to draw two lines from end of each line segment
   float origAngle = atan2(-y2 + y1, x2 - x1);
   float angle1 = origAngle + pi / 5;
   float angle2 = origAngle - pi / 5;
   tree(x2, y2, x2 + .65 * len * cos(angle1), y2 - .65 * len * sin(angle1));
   tree(x2, y2, x2 + .65 * len * cos(angle2), y2 - .65 * len * sin(angle2));
}

// Create fern
void fern(float x1, float y1, float x2, float y2){
   // Base case for length of line segment
   float len = sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2));
   if(len < 3){
      return;
   }
   // Draw line segment
   gfx_line(x1, y1, x2, y2);
   // Recursive step to draw six lines from each line segment
   float origAngle = atan2(-y2 + y1, x2 - x1);
   float angle1 = origAngle + pi / 5;
   float angle2 = origAngle - pi / 5;
   for(int i = 1; i <= 4; i++){
      float a = (x2 - x1) / 4;
      float b = (y2 - y1) / 4;
      fern(x1 + a * i, y1 + b * i, x1 + a * i + .35 * len * cos(angle1), y1 + b * i - .35 * len * sin(angle1));
      fern(x1 + a * i, y1 + b * i, x1 + a * i + .35 * len * cos(angle2), y1 + b * i - .35 * len * sin(angle2));
   }
}

// Create spiral of spirals
void spiralOfSpirals(float xc, float yc, float r, float angle){
   // Base case for radius of spiral
   if(r < 1){
      return;
   }
   // Draw point
   float x = xc + r * cos(angle);
   float y = yc - r * sin(angle);
   gfx_point(x, y);
   // Recursive step to draw next spiral and spiral at current point
   spiralOfSpirals(xc, yc, r * 0.9, angle - pi / 5);
   spiralOfSpirals(x, y, r * 0.35, angle);
}

// Draw a triangle given endpoints
void drawTriangle(int x1, int y1, int x2, int y2, int x3, int y3){
   gfx_line(x1, y1, x2, y2);
   gfx_line(x2, y2, x3, y3);
   gfx_line(x3, y3, x1, y1);
}

// Draw a square given endpoints
void drawSquare(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4){
   gfx_line(x1, y1, x2, y2);
   gfx_line(x2, y2, x3, y3);
   gfx_line(x3, y3, x4, y4);
   gfx_line(x4, y4, x1, y1);
}
