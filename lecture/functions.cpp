// 9/11/17

#include <iostream>
using namespace std;

// Declare add function (prototype)
int add(int, int);
void greetings(void);

int main() { 
   int a, b, c;

   greetings(); // No argument but needs parantheses

   a = 6;
   b = 9;
   c = add(a, b);

   cout << "The sum is: " << c << endl;

   int x = 4, y = 7, z;
   z = add(x, y);
   cout << "The other sum is: " << z << endl;

   if (a == 4){
      return 1;
   }
   // Can write as if (a == 6) return 1;

   greetings();

   return 0;
}

// Add function
int add(int a, int b) {
   int c;
   c = a + b;
   return c;
}

void greetings(void){
   cout << "Hello all!" << endl;
}
