#include <iostream>

using namespace std;

int main(){

   // Read in input (length and width) & initialize
   int length = 0, width = 0;
   
   // Prompt the user for input
   cout << "Enter length and width: ";

   cin >> length;
   cin >> width;

   // Check that input is collected
   cout << "You entered length: " << length << " and width: " << width << endl;

   // Tell user what input was accepted

   // Compare length and width
   cout << "Comparing (length < width): " << (length < width) << endl;
   cout << "(2*length == width)" << (2*length == width) << endl;
   bool result = false;
   result = (2*length == width);
   cout << "Not result: " << !result << endl;

   // Using and - && ; or - ||
   cout << "Length is between 0 and 10: " << ((0 < length) && (length < 10)) << endl;

   cout << "------------------------------------" << endl;

   // Using an if statement
   if (0 < length){
      cout << "Length is positive!" << endl;
      if (length < 10){
         cout << " and length is less than 10." << endl;
      }
   }
   else if (length == 0){
      cout << "Length is zero." << endl;
   }   
   else {
      cout << "Length is negative!" << endl;
   return 0;
   }
}
