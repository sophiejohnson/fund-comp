// Wednesday, 9/6/17
// Using while loops

#include <iostream>

using namespace std;

int main(){

   // Predicting when future tuition will double
   double tuition = 100000;
   double rate = 1.07;
   int year = 1;

   cout << "Year       Tuition" << endl;
   cout << "------------------" << endl;

   while (tuition < 200000){

   tuition = tuition * rate;
   cout << year << "          " << tuition << endl;
   year++;

   } // End of while loop

   cout << "Tuition exceeds double after " << (year - 1) << " years." << endl;

   return 0;

}

