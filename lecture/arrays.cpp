// 9/18/17
// Using for loop and while loop to store user input in an array

#include <iostream>
using namespace std;

int main() {
   int a[21], i = 0, sum = 0;

   cout << "Enter some positive numbers (max 20), followed by a -1: ";

   cin >> a[i];

   // When number of inputs is not known
   while (a[i] > 0){
      sum += a[i];
      i++;
      cin >> a[i];
   }

/*
   // When number of inputs is known
   for (int i = 1; i < 5; i++) {
      cin >> a[i];
      sum += a[i];
   }
*/

   cout << "The sum is: " << sum << endl;
   cout << "There are " << i - 1 << " numbers." << endl;

return 0;
}
