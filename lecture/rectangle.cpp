#include <iostream>
using namespace std;

int main(){
   double w, h, area, perimeter;
   
   cout << "Enter the width and height of a rectangle: ";
   cin >> w  >> h;
   
      area = w * h;
      perimeter = 2 * (w + h);
      
      cout << "The area is: " << area << endl;
      cout << "The perimeter is: " << perimeter << endl;
      
   return 0;
}
