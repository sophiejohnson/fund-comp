#include <iostream>
#include <array>
using namespace std;

int size = 5;
int main(){

   array<int, size> arr = {1, 2, 3, 4, 5};
   for (int i = 1; i < arr.size(); i++)
      cout << arr[i] << " ";
   cout << endl;

   return 0;
}
