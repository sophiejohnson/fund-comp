#include <iostream>
using namespace std;

void compute(double, double, double &, double &);
void display(double, double); 

int main(){
   double w, h, area, perimeter;
   
   cout << "Enter the width and height of a rectangle: ";
   cin >> w  >> h;
   
   compute(w, h, area, perimeter);
   
   display(area, perimeter);
   
   return 0;
}

void compute(double w, double h, double &a, double &p){
   a = w * h;
   p = 2 * (w + h);
}

void display(double a, double p){
   cout << "The area is: " << a << endl;
   cout << "The perimeter is: " << p << endl;
}
