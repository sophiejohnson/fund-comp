// 9/25/17
// Example with char data types and c (constant) strings

#include <iostream>
#include <cstring>
using namespace std;

int main(){
   char c = 'f';
   int n = c;
   cout << c << endl;
   cout << n << endl;
   
   c = c + 3;
   cout << c << endl;
   
   char word[20] = "hi there"; // With tab \t
   cout << word << endl;
   // word[5] = 'J'; // Replaces e with J
   // word[5] = \a; // Alarm
   // word[5] = 13; // ASCII table gives 13 = return cursor to beginning of line
   // word[5] = 8; // Backspace
   cout << word << endl;
   
   for (int i = 0; i < strlen(word); i++){
      word[i]++;
   }
   cout << word << endl;
   int m = c - 'a'; // Gives distance between the character and the first letter of the alphabet
   cout << m << endl; // Outputs 8, shows that i is the 9th letter of the alphabet
   
   return 0;
}
