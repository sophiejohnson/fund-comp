// 9/22/17

#include <iostream>
using namespace std;

void swap(double &, double &);

int main(){
   double a, b;
   a = 12;
   b = 19;
   cout << a << " " << b << endl;
   swap(a,b);
   cout << a << " " << b << endl;
}

void swap(double &x, double &y){
   double temp = x;
   x = y;
   y = temp;
}
