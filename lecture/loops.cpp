// Wednesday, 9/4/17
// Using for and while loops

#include <iostream>

using namespace std;

int main(){

   int n = 0;

   // While loop example
   while(n < 10){
      cout << "N: " << n << endl;
      n = n + 1; 
   }

   // For loop example
   for (int i = 0; i < 10; i++){
      cout << "I: " << i << endl;
   }

   // Nested for loop
   for (int i = 0; i < 5; i++){
      for (int j = 0; j <3; j++){
         if (j == 0){
            cout << "**" << endl;
         }
         else if (j == 1){
            cout << "****" << endl;
         }
         else {
	    cout << "*******" << endl;
	 }
      }
   cout << "_______" << endl;
   }

   // More with for loops (multiplication table: use iteration and conditionals)
   for (int i = 0; i < 5; i++){
      cout << endl;
      for (int j = 0; j < 3; j++){
         cout << "  " << i << "," << j;
      } // End of loop with j
   } // End of loop with i

   // While loop with user input

   char answer = 'y';
   int counter = 0;

   while(answer == 'y'){      
      counter++;
      cout << endl;
      cout << "Continue? [y/n]:";
      cin >> answer;
   } // End of while loop

   return 0;
}
