// Sophie Johnson
// CSE 20311
// 10/23/17
// This program tests the rational class.

#include <iostream>
#include "rational.h"
using namespace std;

int main(){
   
   // Prompt user to enter rational number
   cout << "Enter a numerator and denominator: ";
   int a, b;
   cin >> a >> b;
   Rational rat1 = Rational(a, b);
   
   // Display rational number
   cout << "\tA = ";
   rat1.print();
   
   // Output either the numerator or denominator
   cout << "Choose 1) numerator or 2) denominator: ";
   int userChoice;
   cin >> userChoice;
   while (userChoice != 1 && userChoice != 2){
      cout << "Please enter 1 or 2: ";
      cin >> userChoice;
   }
   if (userChoice == 1){
      cout << "\tNumerator: " << rat1.getNumer() << endl;
   }
   else if (userChoice == 2){
      cout << "\tDenominator: " << rat1.getDenom() << endl;
   }
   
   // Create and display default rational number
   Rational rat2 = Rational();
   cout << "Default rational number: \n\tB = ";
   rat2.print();

   // Change rational number numerator
   cout << "Please enter a new numerator for the default number: ";
   cin >> a;
   rat2.setNumer(a);
   cout << "\tB = ";
   rat2.print();
   
   // Change rational number denominator
   cout << "Please enter a new denominator for the default number: ";
   cin >> b;
   rat2.setDenom(b);
   cout << "\tB = ";
   rat2.print();

   // Test all operations
   Rational sum = rat1.add(rat2);
   Rational difference = rat1.subtract(rat2);
   Rational product = rat1.multiply(rat2);
   Rational quotient = rat1.divide(rat2);

   // Display results of operations
   cout << "A + B = ";
   sum.print();
   cout << "A - B = ";
   difference.print();
   cout << "A x B = ";
   product.print();
   cout << "A / B = ";
   quotient.print();

   return 0;
}
