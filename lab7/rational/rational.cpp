// Sophie Johnson
// CSE 20311
// 10/26/17

#include "rational.h"
#include <iostream>
using namespace std;

Rational::Rational(){
   numer = 1;
   denom = 1;
}

Rational::Rational(int n, int d){
   numer = n;
   denom = d;
}

Rational::~Rational(){
}

int Rational::getNumer(){
   return numer;
}

int Rational::getDenom(){
   return denom;
}

void Rational::setNumer(int n){
   numer = n;
}

void Rational::setDenom(int d){
   denom = d;
}

void Rational::print(){
   cout << numer << "/" << denom << endl;
}

Rational Rational::add(Rational a){
   return Rational(numer * a.getDenom() + a.getNumer() * denom, a.getDenom() * denom);
}

Rational Rational::subtract(Rational a){
   return Rational(numer * a.getDenom() - a.getNumer() * denom, a.getDenom() * denom);
}

Rational Rational::multiply(Rational a){
   return Rational(numer * a.getNumer(), denom * a.getDenom());
}

Rational Rational::divide(Rational a){
   return Rational(numer * a.getDenom(), denom * a.getNumer());
}
