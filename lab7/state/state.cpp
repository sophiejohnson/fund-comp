// Sophie Johnson
// CSE 20311
// 10/28/17
// Implementation for the state class

#include <iostream>
#include <string>
#include "state.h"
using namespace std;

State::State(){
}

State::State(string ab, string nm, string cp, int pp, int yr, int rp){
   abbreviation = ab;
   name = nm;
   capital = cp;
   population = pp;
   year = yr;
   congressReps = rp;
}

State::~State(){
}

void State::setAbbreviation(string ab){
   abbreviation = ab;
}

void State::setName(string nm){
   name = nm;
}

void State::setCapital(string cp){
   capital = cp;
}

void State::setPopulation(int pp){
   population = pp;
}

void State::setYear(int yr){
   year = yr;
}

void State::setCongressReps(int rp){
   congressReps = rp;
}

string State::getAbbreviation(){
   return abbreviation;
}

string State::getName(){
   return name;
}

string State::getCapital(){
   return capital;
}

int State::getPopulation(){
   return population;
}

int State::getYear(){
   return year;
}

int State::getCongressReps(){
   return congressReps;
}

// Method to print state name with abbreviation
void State::printName(){
   cout << name << " (" << abbreviation << ")" << endl;
}

// Method to print all info stored about state
void State::printInfo(){
   printName();
   cout << "Capital: " << capital << endl;
   cout << "Population: " << population << endl;
   cout << "Year of Statehood: " << year << endl;
   cout << "Number of Congress Representatives: " << congressReps << endl;
}

// Method to compare input string to actual capital
bool State::checkCapital(string cp){
   string userCap;
   for (int i = 0; i < cp.length(); i++){
      userCap.push_back(tolower(cp[i]));
   }
   string cap;
   for (int i = 0; i < capital.length(); i++){
      cap.push_back(tolower(capital[i]));
   }
   return (userCap.compare(cap));
}

// Method to find ration between population and congress reps
double State::ratio(){
   return((double)population / (double)congressReps);
}
