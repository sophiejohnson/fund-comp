// Sophie Johnson
// CSE 20311
// 10/28/17
// This program connects to a file name entered by the user and reads
// the data into a vector using the State class. Then, it prompts the
// user to choose an action: display all states, find more information
// about a specific state, find states with a minimum population,
// guess the capital of a state, find the ratio between population and
// congress resps, or exit.

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <vector>
using namespace std;

#include "state.h"

// Declare functions
void dispOptions(void);
void disp(vector<State> &);
void moreInfo(vector<State> &);
void minPopulation(vector<State> &);
void guessCapital(vector<State> &);
void findRatio(vector<State> &);

int main(){

   // Initialize variables
   ifstream ifs;
   string fileName;
   string abb, nm, cap, pop_str, yr_str, reps_str;
   int pop, yr, reps;
   vector<State> states;
   State state;
   int userChoice = 0;
   bool endProgram = false;
   
   // Prompt user for file name and verify connection
   cout << "Please enter data file name: ";
   cin >> fileName;
   ifs.open(fileName);
   if (!ifs){
      cout << "ERROR: Unable to connect to file." << endl;
      return 1;
   }

   // Read data into vector
   while(ifs.peek() != EOF){
      getline(ifs, abb, ',');
      getline(ifs, nm, ',');
      getline(ifs, cap, ',');
      getline(ifs, pop_str, ',');
      pop = stoi(pop_str);
      getline(ifs, yr_str, ',');
      yr = stoi(yr_str);
      getline(ifs, reps_str, '\n');
      reps = stoi(reps_str);
      // Store each line of data file as a State
      state = State(abb, nm, cap, pop, yr, reps);
      states.push_back(state);
   }

   // Prompt user to choose option
   while (!endProgram){
      dispOptions();
   
      // Verify user input
      cin >> userChoice;
      while ((userChoice < 1) || (userChoice > 6)){
         cout << "Please enter a choice between 1 and 6: ";
         cin >> userChoice;
      }
   
      // Perform appropriate action
      switch (userChoice){
         // Display all states
         case 1:
            disp(states);
            break;
         // Display info about specific state
         case 2:
            moreInfo(states);
            break;
         // Find states with minimum population
         case 3:
            minPopulation(states);
            break;
         // Have user guess capital
         case 4:
	        guessCapital(states);
            break;
         // Find population to congress reps ratio
         case 5:
            findRatio(states);
            break;
         // Make exit condition for while loop true
         case 6:
            endProgram = true;
      }
   }
   return 0;
}

// Function to display options
void dispOptions(void){
   cout << endl;
   cout << "1) Display all state names with abbreviations" << endl;
   cout << "2) Learn more about a specific state" << endl;
   cout << "3) Find all states with a minimum population" << endl;
   cout << "4) Test your knowledge of a state capital" << endl;
   cout << "5) Find ratio between state population and representatives in congress" << endl;
   cout << "6) Exit" << endl;
   cout << "Please choose an option: ";
}

// Function to display all state names and abbreviations
void disp(vector<State> &states){
   int i = 1;
   cout << endl;
   for(auto it = states.begin(); it != states.end(); it++){
      cout << setw(2) << i++ << ") ";
      it->printName();
   }
}

// Function to display info about a specific state
void moreInfo(vector<State> &states){
   // Display all options and prompt user to choose state
   disp(states);
   cout << endl;
   cout << "Which state would you like to learn more about? " << endl;
   cout << "Choose a state (1 - 50): ";
   // Record user choice and verify input
   int k;
   cin >> k;
   while ((k < 1) || (k > 50)){
      cout << "Please enter an integer between 1 and 50: ";
      cin >> k;
   }
   cout << endl;
   // Display appropriate information
   states[k - 1].printInfo();
}

// Function to display states with a minimum population
void minPopulation(vector<State> &states){
   // Prompt user to enter minimum population
   cout << "Enter a minimum population: ";
   // Record and verify user input
   int minPop;
   cin >> minPop;
   while (minPop < 0){
      cout << "Enter a population greater than zero: ";
      cin >> minPop;
   }
   int i = 1;
   bool foundState = false;
   cout << endl;
   // Compare each state population to minimum population
   for (auto it = states.begin(); it != states.end(); it++){
      // If population is larger, display name, abbreviation, and population
      if (it->getPopulation() > minPop){
         cout << i++ << ") ";
         it->printName();
         cout << "\tPopulation: " << it->getPopulation() << endl;
         foundState = true;
      }
   }
   // Output if no states have population greater than minimum
   if (!foundState){
      cout << "No states have a population greater than " << minPop << "." << endl;
   }
}

// Function to guess state capital
void guessCapital(vector<State> &states){
   // Display all states and prompt user to choose state
   disp(states);
   cout << endl;
   cout << "What state capital would you like to guess? " << endl;
   cout << "Choose a state (1 - 50): ";
   // Record and verify user input
   int k;
   cin >> k;
   while ((k < 1) || (k > 50)){
      cout << "Please enter an integer between 1 and 50: ";
      cin >> k;
   }
   // Prompt user to guess capital and record guess
   cout << "Guess the capital of " << states[k - 1].getName() << ": ";
   string guess;
   cin.ignore();
   getline(cin, guess);
   // Output if guess is correct
   if (states[k - 1].checkCapital(guess) == 0){
      cout << "Correct! The capital of " << states[k - 1].getName();
      cout << " is " << states[k - 1].getCapital() << "." << endl;
   }
   // Output if guess is incorrect
   else{
      cout << "Incorrect. The capital of " << states[k - 1].getName();
      cout << " is " << states[k - 1].getCapital() << "!" << endl;
   }
}

// Function to find ratio of congress reps to population
void findRatio(vector<State> &states){
   // Display all states and prompt user to choose state
   disp(states);
   cout << endl;
   cout << "Choose a state (1 - 50): ";
   // Record and verify user input
   int k;
   cin >> k;
   while ((k < 1) || (k > 50)){
      cout << "Please enter an integer between 1 and 50: ";
      cin >> k;
   }
   // Display state name, abbreviation, and ratio
   states[k - 1].printName();
   cout << "Ratio between population and representatives in congress: ";
   cout << setprecision(2) << fixed;
   cout << states[k - 1].ratio() << endl;
}
