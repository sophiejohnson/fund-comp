// Sophie Johnson
// CSE 20311
// 10/28/17
// Interface for the state class

#include <string>
using namespace std;

class State{
   public:
      State();
      State(string, string, string, int, int, int);
      ~State();
      void setAbbreviation(string);
      void setName(string);
      void setCapital(string);
      void setPopulation(int);
      void setYear(int);
      void setCongressReps(int);
      string getAbbreviation();
      string getName();
      string getCapital();
      int getPopulation();
      int getYear();
      int getCongressReps();
      void printName();
      void printInfo();
      bool checkCapital(string);
      double ratio();
   private:
      string abbreviation;
      string name;
      string capital;
      int population;
      int year;
      int congressReps;
};
