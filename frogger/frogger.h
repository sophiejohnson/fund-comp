// Sophie Johnson
// CSE 20311
// 12/5/17
// Interface for the Frogger class

#include <vector>
using namespace std;

class Frogger{
   public:
      Frogger(int, int, int);
      ~Frogger();
      void setWd(int);
      void setHt(int);
      void setStep(int);
      void setLives(int);
      void setScore(int);
      void setTime(int);
      void setCars();
      void setLogs();
      void setLilypads();
      int getWd();
      int getHt();
      int getStep();
      int getLives();
      int getScore();
      int getTime();
      void forwardProgress();
      void updateGame();
      void dispBackground();
      void dispStats();
      void dispObstacle(int[5][5], int);
      void dispLilypads();
      void drawRectangle(int, int, int, int, int);
      bool checkCollisions(int, int, int);
      bool checkDrowning(int, int, int &);
      bool lilypadUpdate(int, int, int &);
      bool winGame();
      bool loseGame();
   private:
      int wd;
      int ht;
      int step;
      int lives;
      int score;
      int time;
      int cars[5][5];
      int logs[5][5];
      int lilypads[5][4];
};
