// Sophie Johnson
// CSE 20311
// 12/5/17
// Final Project

#include <unistd.h>
#include <cstdlib>
#include <ctime>
#include "gfxnew.h"
#include "frogger.h"

int main(){

   // Set dimensions and open window
   int wd = 600, ht = 640, step = wd / 15;
   gfx_open(wd, ht, "Frogger");

   // Set parameters
   char c = 0;
   char titleFont[] = "12x24";
   char mainFont[] = "8x16";
   bool endGame = false;
   int deltat = 12500, deltax = 0, event;
   int x = wd / 2, y = ht - 3 * step / 2, r = step / 3;

   // Title screen prompts user to press space
   while(c != ' '){
      gfx_changefont(titleFont);
      gfx_text(wd / 2 - 50, 300, "FROGGER");
      gfx_changefont(mainFont);
      gfx_text(wd / 2 - 160, 320, "Use the arrow keys to navigate the game.");
      gfx_text(wd / 2 - 90, 340, "Press space to begin!");
      c = gfx_wait();
   }

   // Record initial time and y position
   int t0 = time(NULL), yMin = y;

   // Create Frogger game
   Frogger game(wd, ht, step);
   bool hit = false;
   while(!endGame){

      // Animate screen
      gfx_clear();
      game.updateGame();
      gfx_fill_circle(x, y, r);
      x += deltax;
      game.setTime(60 - difftime(time(NULL), t0));
      gfx_flush();
      usleep(deltat);

      // Respond to user input
      if((event = gfx_event_waiting()) != 0){
         c = gfx_wait();
         if(event == 1){
            switch(c){
               // Up arrow
               case 'R':
                  if(y > 2 * step){
                     y -= step;
                  }
                  break;
               // Down arrow
               case 'T':
                  if(y < ht - 2 * step){
                     y += step;
                  }
                  break;
               // Right arrow
               case 'S':
                  if(x < wd- step){
                     x += step;
                  }
                  break;
               // Left arrow
               case 'Q':
                  if(x > step){
                     x -= step;
                  }
                  break;
               // Quit if user presses 'q'
               case 'q':
                  endGame = true;
                  break;
            }
         }
      }

      // Check if frog gets hit by car or drowns
      if(game.checkCollisions(x, y, r) || game.checkDrowning(x, y, deltax)){
         // Return to original position
         x = wd / 2;
         y = ht - 3 * step / 2;
         yMin = y;
      }

      // If forward progress, add to score
      else if(y < yMin){
         game.forwardProgress();
         yMin = y;
      }

      // Update score and screen if frog lands on lilypad
      if(game.lilypadUpdate(x, y, t0)){
         // Return to original position
         x = wd / 2;
         y = ht - 3 * step / 2;
         yMin = y;
      }

      // Check to see if user lost game
      if(game.loseGame()){
         endGame = true;
         usleep(1000000);
         gfx_clear();
         // Display final stats and prompt user to press space
         while(c != ' '){
            game.dispStats();
            gfx_text(wd / 2 - 170, 320, "Game over, you lost! Press space to exit.");
            c = gfx_wait();
         }
      }

      // Check to see if user won game
      else if(game.winGame()){
         // Return to original position, update display, and pause
         x = wd / 2;
         y = ht - 3 * step / 2;
         gfx_clear();
         gfx_fill_circle(x, y, r);
         game.updateGame();
         gfx_flush;
         endGame = true;
         usleep(1000000);
         gfx_clear();
         // Display final stats and prompt user to press space
         while(c != ' '){
            game.dispStats();
            gfx_text(wd / 2 - 190, 320, "Congratulations, you won! Press space to exit.");
            c = gfx_wait();
         }
      }
   }
}
