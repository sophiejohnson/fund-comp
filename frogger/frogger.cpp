// Sophie Johnson
// CSE 20311
// 12/5/17
// Implementation for the Frogger class

#include <cmath>
#include "gfxnew.h"
#include "frogger.h"
using namespace std;

// Initialize all parameters in constructor
Frogger::Frogger(int wd, int ht, int step){
   setWd(wd);
   setHt(ht);
   setStep(step);
   setLives(3);
   setScore(0);
   setTime(60);
   setCars();
   setLogs();
   setLilypads();
}

Frogger::~Frogger(){
}

void Frogger::setWd(int a){
   wd = a;
}

void Frogger::setHt(int a){
   ht = a;
}

void Frogger::setStep(int a){
   step = a;
}

void Frogger::setLives(int a){
   lives = a;
}

void Frogger::setScore(int a){
   score = a;
}

void Frogger::setTime(int a){
   time = a;
}

// Create 2D array of cars
void Frogger::setCars(){
   // Array of x positions, y scalars, sizes, and dx values
   int carsNew[5][5] = {{100, 400, ht - 13 * step / 2, 100, 2},
   				       {200, 400, ht - 11 * step / 2, 40, -3},
   				       {300, 175, ht - 9 * step / 2, 60, 1},
   				       {400, 100, ht - 7 * step / 2, 60, -1},
   				       {500, 250, ht - 5 * step / 2, 120, 2}};
   // Assign values to cars array
   for(int i = 0; i < 5; i++){
      for(int j = 0; j < 5; j++){
         cars[i][j] = carsNew[i][j];
      }
   }
}

// Create 2D array of logs
void Frogger::setLogs(){
   // Array of x positions, y scalars, sizes, and dx values
   int logsNew[5][5] = {{150, 450, ht - 25 * step / 2, 120, -2},
   				       {100, 250, ht - 23 * step / 2, 80, 2},
   				       {400, 175, ht - 21 * step / 2, 40, -1},
   				       {400, 100, ht - 19 * step / 2, 110, 3},
   				       {400, 150, ht - 17 * step / 2, 70, -2}};
   // Assign values to logs array
   for(int i = 0; i < 5; i++){
      for(int j = 0; j < 5; j++){
         logs[i][j] = logsNew[i][j];
      }
   }
}

// Create 2D array of lilypads
void Frogger::setLilypads(){
   // Create vector with each lilypad's x, y, radius, and occupied values
   int y = 5 * step / 2, r = 3 * step / 8;
   int lilypadsNew[5][4] = {{3*step/2, y, r, 0},
                            {9*step/2, y, r, 0},
                            {15*step/2, y, r, 0},
                            {21*step/2, y, r, 0},
                            {27*step/2, y, r, 0}};
   // Assign values to lilypads array
   for(int i = 0; i < 5; i++){
      for(int j = 0; j < 4; j++){
         lilypads[i][j] = lilypadsNew[i][j];
      }
   }
}

int Frogger::getWd(){
   return wd;
}

int Frogger::getHt(){
   return ht;
}

int Frogger::getStep(){
   return step;
}

int Frogger::getLives(){
   return lives;
}

int Frogger::getScore(){
   return score;
}

int Frogger::getTime(){
   return time;
}

// Add 10 to score for forward progress
void Frogger::forwardProgress(){
   score += 10;
}

// Update all graphics on screen
void Frogger::updateGame(){
   dispBackground();
   dispStats();
   dispObstacle(cars, 0);
   dispObstacle(logs, 1);
   dispLilypads();
}

// Display background elements
void Frogger::dispBackground(){
   gfx_line(0, ht - 13 * step, wd, ht - 13 * step);
   gfx_line(0, ht - 8 * step, wd, ht - 8 * step);
   gfx_line(0, ht - 7 * step, wd, ht - 7 * step);
   gfx_line(0, ht - 2 * step, wd, ht - 2 * step);
}

// Display lives, score, and time at top of screen
void Frogger::dispStats(){
   // Create c string for lives left
   char livesDisp[20] = "Lives: ", livesNum = '0' + lives;
   if(lives < 0){
      livesNum = '0';
   }
   bool numAdded;
   int i = 0;
   while(i < 20 && !numAdded){
      if(livesDisp[i] == '\0'){
         livesDisp[i] = livesNum;
         numAdded = true;
      }
      i++;
   }
   // Create c string for current score
   char scoreDisp[20] = "Score: ";
   numAdded = false;
   i = 0;
   int k = (score == 0)? 1 : log10(score) + 1, scoreNum = score;
   while(i < 20 && !numAdded){
      if(scoreDisp[i] == '\0'){
         for(int j = 1; j <= k; j++){
            scoreDisp[i + k - j] = '0' + scoreNum % 10;
            scoreNum /= 10;
         }
         numAdded = true;
      }
      i++;
   }
   // Create c string for time left
   char timeDisp[20] = "Time: ";
   numAdded = false;
   i = 0;
   k = (time <= 0)? 1 : log10(time) + 1;
   int timeNum = time;
   while(i < 20 && !numAdded){
      if(timeDisp[i] == '\0'){
         for(int j = 1; j <= k; j++){
            timeDisp[i + k - j] = '0' + timeNum % 10;
            timeNum /= 10;
         }
         numAdded = true;
      }
      i++;
   }
   // Display lives, score, and time
   gfx_text(step / 2, step / 2 + 10, livesDisp);
   gfx_text(wd / 2 - 35, step / 2 + 10, scoreDisp);
   gfx_text(wd - 75, step / 2 + 10, timeDisp);
   gfx_line(0, step, wd, step);

}

// Display obstacle (cars or logs) moving across screen
void Frogger::dispObstacle(int a[5][5], int c){
   for(int i = 0; i < 5; i++){
      // Animate motion of obstacle
      drawRectangle(a[i][0], a[i][2], a[i][3], 2 * step / 3, c);
      drawRectangle(a[i][1], a[i][2], a[i][3], 2 * step / 3, c);
      a[i][0] += a[i][4];
      a[i][1] += a[i][4];
      // Loop if obstacle moves off edge of screen
      a[i][0] = (a[i][0] < -60)? wd + 60 : a[i][0];
      a[i][0] = (a[i][0] > wd + 60)? -60 : a[i][0];
      a[i][1] = (a[i][1] < -60)? wd + 60 : a[i][1];
      a[i][1] = (a[i][1] > wd + 60)? -60 : a[i][1];
   }
}

// Display lilypads
void Frogger::dispLilypads(){
   for(int i = 0; i < 5; i++){
      gfx_circle(lilypads[i][0], lilypads[i][1], lilypads[i][2]);
      // If lilypad is occupied, draw smaller circle inside
      if(lilypads[i][3] == 1){
         gfx_fill_circle(lilypads[i][0], lilypads[i][1], step / 3);
      }
   }
   gfx_flush();
}


// Draw rectangles (for cars or logs) given center coordinates and dimensions
void Frogger::drawRectangle(int xc, int yc, int wd, int ht, int c){
   int a = wd / 2, b = ht / 2;
   switch(c){
      // Draw car rectangle
      case 0:
         gfx_rectangle(xc - a, yc - b, wd, ht);
         gfx_fill_rectangle(xc - a * 0.5 , yc - b * 0.65, wd * 0.5, ht * 0.7);
         break;
      // Draw log rectangle
      case 1:
         gfx_rectangle(xc - a, yc - b, wd, ht);
         gfx_line(xc - a, yc - b / 2, xc + a, yc - b / 2);
         gfx_line(xc - a, yc, xc + a, yc);
         gfx_line(xc - a, yc + b / 2, xc + a, yc + b / 2);
         break;
   }
   gfx_flush();
}

// Check to see if frog collides with cars
bool Frogger::checkCollisions(int x, int y, int r){
   bool collide = false;
   for(int i = 0; i < 5; i++){
      // Only check lines with cars
      if(y == cars[i][2]){
         // Check with front and back of first car in line
         if(x - r <= cars[i][0] + cars[i][3] / 2 && x - r >= cars[i][0] - cars[i][3] / 2){
            collide = true;
         }
         else if(x + r >= cars[i][0] - cars[i][3] / 2 && x + r <= cars[i][0] + cars[i][3] / 2){
            collide = true;
         }
         // Check with front and back of second car in line
         else if(x - r <= cars[i][1] + cars[i][3] / 2 && x - r >= cars[i][1] - cars[i][3] / 2){
            collide = true;
         }
         else if(x + r >= cars[i][1] - cars[i][3] / 2 && x - r <= cars[i][1] + cars[i][3] / 2){
            collide = true;
         }
      }
   }
   // If collision, lose life
   if(collide){
      lives -= 1;
   }
   return collide;
}

// Check to see if frog drowns
bool Frogger::checkDrowning(int x, int y, int &dx){
  dx = 0;
  bool drown = false;
  for(int i = 0; i < 5; i++){
      // Only check lines with logs
      if(y == logs[i][2]){
         // If on first log in line, record speed
         if(x <= logs[i][0] + logs[i][3] / 2 && x >= logs[i][0] - logs[i][3] / 2){
            dx = logs[i][4];
         }
         // If on second log in line, record speed
         else if(x <= logs[i][1] + logs[i][3] / 2 && x >= logs[i][1] - logs[i][3] / 2){
            dx = logs[i][4];
         }
         // If frog isn't on log, it drowned
         else{
            drown = true;
         }
      }
   }
   // If drowning, lose life
   if(drown){
      lives -= 1;
   }
   return drown;
}

// Check to see if user landed on or missed lilypad
bool Frogger::lilypadUpdate(int x, int y, int &t0){
   bool reachEnd = false, success = false;
   for(int i = 0; i < 5; i++){
      // Only check line with lilypads
      if(y == lilypads[i][1]){
         reachEnd = true;
         // Check if landed on lilypad
         if(x <= lilypads[i][0] + lilypads[i][2] && x >= lilypads[i][0] - lilypads[i][2]){
            // If lilypad is unoccupied, change to occupied/increase score/add 10 seconds
            if(lilypads[i][3] == 0){
               lilypads[i][3] = 1;
               score += 50;
               t0 += 10;
               success = true;
            }
         }
      }
   }
   // If user missed lilypad, lose a life
   if(reachEnd && !success){
      lives -= 1;
   }
   return reachEnd;
}

// If user lands frog on every lilypad, win game
bool Frogger::winGame(){
   bool winGame = true;
   for(int i = 0; i < 5; i++){
      // If lilypad is unoccupied, user hasn't won
      if(lilypads[i][3] == 0){
         winGame = false;
      }
   }
   // If user wins, add 1000 to score and 10 x remaining time
   if(winGame){
      score += 1000 + 10 * time;
   }
   return winGame;
}

// If ran out of lives or time, user loses
bool Frogger::loseGame(){
   return (lives < 0 || time < 0)? true : false;
}
