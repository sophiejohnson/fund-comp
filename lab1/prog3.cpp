// Sophie Johnson   
// CSE20311
// 8/30/17
// Part	3: Formula Calculator
// This	program	collects input from the user in order to calculate the
// force between two point charges using Coulomb's Law.

#include <iostream>
#include <cmath>
using namespace std;

int main()
{

// Initialize variables
double k;
k = 8.988 * pow(10,9);
double q1;
double q2;
double r;

// Collect input from user
cout << "Hello!  This program calculates the force between two point charges.\n";
cout << "What is the charge of Point 1 (in Coulombs)?\n";
cin >> q1;
cout << "What is the charge of Point 2 (in Coulombs)?\n";
cin >> q2;
cout << "How far apart are Point 1 and Point 2 (in meters)?\n";
cin >> r;

// Calculate force between point forces
double force;
force = (k*q1*q2)/(pow(r,2));

// Display results
cout << "The force between Point 1 and Point 2 is " << force << " N!\n";

return 0;
}
