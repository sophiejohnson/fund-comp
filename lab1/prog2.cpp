// Sophie Johnson   
// CSE20311
// 8/30/17
// Part	2: Football Score Formula
// This	program asks a user the number of touchdowns, extra points, field
// goals, and safeties.  It then calculates and displays the final score.

#include <iostream>
using namespace std;

int main()
{

// Record results of game
     int touchdowns, extraPoints, fieldGoals, safeties;     
     cout << "How many touchdowns did the Irish score?\n";
     cin >> touchdowns;
     cout << "How many extra points?\n";
     cin >> extraPoints;
     cout << "How many field goals?\n";
     cin >> fieldGoals;
     cout << "How many safeties?\n";
     cin >> safeties;

// Calculate total score
     int totalPoints;
     totalPoints = 6*touchdowns + 1*extraPoints + 3*fieldGoals + 2*safeties; 
  
// Display results
     cout << "The Irish scored " << totalPoints;
     cout << " points!\n";

return 0;
}
