// Sophie Johnson
// CSE 20311
// 11/29/17
// Part 2: Bouncing Ball
// This program displays the animation of a circle moving
// with a random constant velocity that bounces when it collides
// with the boundaries of the window. When the user clicks,
// the circle moves to the location of the cursor and starts to
// move with a new random velocity.

#include <unistd.h>
#include <cstdlib>
#include <time.h>
#include "gfx.h"

int main(){
   // Set window dimensions
   int wd = 600, ht = 400;
   // Set initial position and radius of circle
   double x = 300, y = 200, rad = 25;
   // Generate random initial velocity
   srand(time(NULL));
   double dx = (rand() / double(RAND_MAX)) * 11 - 5;
   double dy = (rand() / double(RAND_MAX)) * 11 - 5;
   // Time between each animation
   int deltat = 6000;
   char c = 0;
   // Open window
   gfx_open(wd, ht, "Bouncing Ball");
   // Animate bouncing ball
   while(c != 'q'){
      gfx_clear();
      x += dx;
      y += dy;
      gfx_circle(x, y, rad);
      gfx_flush();
      usleep(deltat);
      // If circle collides with left edge
      if(x <= rad){
         dx *= -1;
         x = rad + 1;
      }
      // If circle collides with right edge
      else if(x >= wd - rad){
         dx *= -1;
         x = wd - rad - 1;
      }
      // If circle collides with top edge
      if(y <= rad){
         dy *= -1;
         y = rad + 1;
      }
      // If circle collides with bottom edge
      else if(y >= ht - rad){
         dy *= -1;
         y = ht - rad - 1;
      }
      // Check for user mouse click
      if(gfx_event_waiting()){
         c = gfx_wait();
         if(c == 1){
            // Move ball to position of mouse
            x = gfx_xpos();
            y = gfx_ypos();
            // Generate random velocity
            srand(time(NULL));
            dx = (rand() / double(RAND_MAX)) * 11 - 5;
            dy = (rand() / double(RAND_MAX)) * 11 - 5;
         }
      }
   }
   return 0;
}
