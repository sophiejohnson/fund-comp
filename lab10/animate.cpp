// Sophie Johnson
// CSE 20311
// 11/30/17
// Part 3: Rotating Animation
// This program displays a shape moving in a path around a point. It
// then allows to user to choose an action: change position of the
// center point, change shape, change color, change size of shape,
// change size of loop, and quit.

#include <unistd.h>
#include <cstdlib>
#include <time.h>
#include <cmath>
#include "gfx.h"

void setNumSides(int &);
void setColor(char);
void drawShape(int, int, int, int);
void drawPolygon(int, int, int, int);
void showInstructions(void);

const float pi = 3.141592653598;

int main(){
   // Set window dimensions
   int wd = 500, ht = 500;
   // Set parameters
   int numSides = 2, shapeRad = 20, loopRad = 100;
   float xc = 250, yc = 250, x, y, deltat = 5000;
   float angle = 0, angleIncrement = 2 * pi / 360;
   char c = 0;
   bool doExit = false;
   // Open window
   gfx_open(wd, ht, "Rotating Animation");
   // Wait for user to click screen
   while(c != 1){
      gfx_text(10, 20, "Click the screen to begin!");
      c = gfx_wait();
   }
   // Record position of cursor as center point
   xc = gfx_xpos();
   yc = gfx_ypos();
   // Animate circular motion around center point
   while(c != 'q'){
      gfx_clear();
      x = xc + loopRad * cos(angle);
      y = yc - loopRad * sin(angle);
      angle += angleIncrement;
      drawShape(x, y, shapeRad, numSides);
      showInstructions();
      gfx_flush();
      usleep(deltat);
      if(gfx_event_waiting()){
         c = gfx_wait();
         switch(c){
            // Change position of center point with mouse click
            case 1:
               xc = gfx_xpos();
               yc = gfx_ypos();
               break;
            // Change shape with space bar
            case ' ':
               setNumSides(numSides);
               break;
            // Change color with 1 - 6
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
               setColor(c);
               break;
            // Increase shape size with up arrow
            case 'R':
               if(shapeRad < 100){
                  shapeRad += 3;
               }
               break;
            // Decrease shape size with down arrow
            case 'T':
               if(shapeRad > 4){
                  shapeRad -= 3;
               }
               break;
            // Increase loop size with right arrow
            case 'S':
               if(loopRad < 200){
                  loopRad += 3;
               }
               break;
            // Decrease loop size with left arrow
            case 'Q':
               if(loopRad > 50){
                  loopRad -= 3;
               }
               break;
         }
      }
   }
}

// Function to randomly set number of sides of shape
void setNumSides(int &numSides){
   numSides = rand() % 6 + 2;
}

// Function to set color
void setColor(char color){
   switch(color){
      case '1':
         gfx_color(226, 76, 56);
         break;
      case '2':
         gfx_color(255, 185, 56);
         break;
      case '3':
         gfx_color(181, 216, 41);
         break;
      case '4':
         gfx_color(42, 185, 221);
         break;
      case '5':
         gfx_color(163, 130, 204);
         break;
      case '6':
         gfx_color(255, 255, 255);
         break;
   }
}

// Function to draw shape depending on number of sides
void drawShape(int x, int y, int rad, int numSides){
   if(numSides == 2){
      gfx_circle(x, y, rad);
   }
   else{
      drawPolygon(x, y, rad, numSides);
   }

}

// Function to draw polygon of any size
void drawPolygon(int x, int y, int rad, int numSides){
   // Record angle between each point and size of shape
   float angle = 0, angleIncrement = (2 * pi) / numSides;
   float x1 = rad * cos(angle), y1 = rad * sin(angle), x2, y2;
   for(int i = 0; i < numSides; i++){
      // Find second point after incrementing angle by calculated value
      angle += angleIncrement;
      x2 = rad * cos(angle), y2 = rad * sin(angle);
      // Draw line between two points
      gfx_line(x + x1, y - y1, x + x2, y - y2);
      // Record the position of the second point as the first point
      x1 = x2;
      y1 = y2;
   }
}

// Function to display all options
void showInstructions(){
   gfx_text(10, 20, "Click anywhere to change position.");
   gfx_text(10, 35, "Press the space bar to change shapes.");
   gfx_text(10, 50, "Press numbers 1 - 6 to change color.");
   gfx_text(10, 65, "Press the up and down arrows to change size of shape.");
   gfx_text(10, 80, "Press the left and right arrows to change size of loop.");
   gfx_text(10, 95, "Press 'q' to quit.");
}
