// Sophie Johnson
// CSE 20311
// 11/27/17
// Part 1: Symbolic Typewriter
// This program opens a window and responds to different user input. A mouse
// click draws a blue square, the letter t draws a green triangle, the letter
// c draws a white circle, the numbers 3 through 9 draw a purple polygon
// with that many sides, the escape key clears the window, and the letter
// q quits the program.

#include <iostream>
#include <cstdlib>
#include <cmath>
#include "gfx.h"
using namespace std;

// Declare functions
void blueSquare(int, int);
void greenTriangle(int, int);
void whiteCircle(int, int);
void purplePolygon(int, int, char);
void drawPolygon(int, int, int);

const float pi = 3.141592653598;

int main(){

   // Set window size and initialize boolean
   const int width = 600, height = 400;
   char c;
   bool endLoop = false;
   // Open new window with default background
   gfx_open(width, height, "Symbolic Typewriter");

   while(!endLoop){
      // Wait for user input
      c = gfx_wait();
      // Record position of cursor
      int x = gfx_xpos();
      int y = gfx_ypos();
      switch(c){
         // Draw blue square outline
         case 1:
            blueSquare(x, y);
            break;
         // Draw green triangle outline
         case 't':
            greenTriangle(x, y);
            break;
         // Draw white circle outline
         case 'c':
            whiteCircle(x, y);
            break;
         // Draw purple polygon outline
         case '3':
         case '4':
         case '5':
         case '6':
         case '7':
         case '8':
         case '9':
            purplePolygon(x, y, c);
            break;
         // Clear screen
         case 27:
            gfx_clear();
            break;
         // Close window and quit program
         case 'q':
            endLoop = true;
            break;
      }
   }
   return 0;
}

// Function to draw blue square
void blueSquare(int x, int y){
   gfx_color(121, 183, 224);
   drawPolygon(x, y, 4);
}

// Function to draw green triangle
void greenTriangle(int x, int y){
   gfx_color(137, 206, 90);
   drawPolygon(x, y, 3);

}

// Function to draw white circle
void whiteCircle(int x, int y){
   gfx_color(255, 255, 255);
   gfx_circle(x, y, 20);
}

// Function to draw purple polygon
void purplePolygon(int x, int y, char c){
   gfx_color(174, 126, 204);
   drawPolygon(x, y, c - '0');
}

// Function to draw polygon of any size
void drawPolygon(int x, int y, int numSides){
   // Record angle between each point and size of shape
   float angleIncrement = (2 * pi) / numSides, r = 25;
   // Choose random position for first point
   float angle = (pi * (double)rand()) / (RAND_MAX);
   float x1 = r * cos(angle), y1 = r * sin(angle), x2, y2;
   for(int i = 0; i < numSides; i++){
      // Find second point after incrementing angle by calculated value
      angle += angleIncrement;
      x2 = r * cos(angle), y2 = r * sin(angle);
      // Draw line between two points
      gfx_line(x + x1, y - y1, x + x2, y - y2);
      // Record the position of the second point as the first point
      x1 = x2;
      y1 = y2;
   }
}
