// Sophie Johnson
// CSE 20311
// 10/11/17
// This program counts the frequency of each letter of the alphabet in
// a text file. The program will output the frequency values for each
// letter, the total number of letters, the total number of characters,
// and the space percentage.

#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>
using namespace std;

void dispLetterFreq(vector<int>);

int main(){

   // Prompt user for text file name
   cout << "Please enter a text file: ";
   string fileName;
   cin >> fileName;
   
   // Verify connection with text file
   ifstream ifs;
   ifs.open(fileName);
   if (!ifs){
      cout << "ERROR: Could not find file." << endl;
      return 1;
   }
   
   // Initialize frequency vector and accumulation variables
   int numLetters = 0, numChars = 0, numSpaces = 0;
   char c;
   vector<int> freq (26,0);
   
   // Find frequency of each character in text file
   ifs.get(c);
   while (!ifs.eof()){
      numChars++;
      // Determine if white space character
      if (isspace(c)){
         numSpaces++;
      }
      // Determine if letter and add to frequency vector
      else if (isalpha(c)){
         numLetters++;
         if (isupper(c)){
            c = tolower(c);
         }
         int position = c - 'a';
         freq[position]++;
      }
      ifs.get(c);
   }
   
   // Output individual letter frequency
   dispLetterFreq(freq);
   
   // Output total number of letters, characters, and white characters
   cout << "The file " << fileName << " contains:" << endl;
   cout << '\t' << numLetters << " letters" << endl;
   cout << '\t' << numChars << " characters" << endl;
   cout << '\t' << numSpaces << " white space characters" << endl;
   cout << setprecision(1) << fixed;
   cout << "The space percentage is " << ((double)numSpaces/(double)numChars) * 100 << "%." << endl;
   return 0;
}

// Function to display letter frequency
void dispLetterFreq(vector<int> freq){
   char i = 'A';
   int numOutputs = 0;
   for (auto it = freq.begin(); it != freq.end(); it++){
      // Adjust spacing based on number of A's
      cout << setw(3) << i++ << ": " << setw(floor(log10(freq[1])) + 2) << *it;
      numOutputs++;
      // Display in six columns
      if (numOutputs >= 6){
         cout << endl;
         numOutputs = 0;
      }
   }
   cout << endl;
}
