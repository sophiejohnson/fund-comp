// Sophie Johnson
// CSE 20311
// 10/7/17
// This program stores sayings. It begins by prompting the user for a
// startup data file. Then, it displays a menu of options for the user:
// display all sayings, add a new saying, list sayings containing a
// specific substring, save all sayings in a new text file, and quit
// the program.

#include <iostream>
#include <fstream>
#include <list>
#include <string>
using namespace std;

// Declare functions
void disp(list<string>);
void add(list<string> &);
void substring(list<string>);
void save(list<string>);
void remove(list<string> &);

int main(){

   // Prompt user for startup data file
   cout << "Please enter a startup text file: ";
   string filename;
   cin >> filename;

   // Verify connection to data file
   ifstream ifs;
   ifs.open(filename);
   if (!ifs){
      cout << "ERROR: File could not be found." << endl;
      return 1;
   }

   // Read sayings from startup data file into vector
   list<string> sayings;
   string newLine;
   getline(ifs, newLine);
   while (!ifs.eof()){
      sayings.push_back(newLine);
      getline(ifs, newLine);
   }
   sayings.sort();

   // While loop until exit condition met
   bool doExit = false;
   while (!doExit){

      // Display options
      cout << endl;
      cout << "1) Display all sayings" << endl;
      cout << "2) Enter a new saying" << endl;
      cout << "3) List all sayings that contain a given word" << endl;
      cout << "4) Save all sayings in new text file" << endl;
      cout << "5) Delete a saying" << endl;
      cout << "6) Quit the program" << endl;
      cout << "Please choose an option: ";

      // Check user input
      int userChoice = 0;
      cin >> userChoice;
      while (userChoice < 1 || userChoice > 6){
         cout << "Please enter an integer between 1 and 5: ";
         cin >> userChoice;
      }
      cout << endl;
      cin.ignore();

      // Perform action based on input
      switch (userChoice){

         // Display all sayings
		 case 1:
		    disp(sayings);
	        break;

         // Add new saying
		 case 2:
		    add(sayings);
		    break;

         // List sayings containing given substring
		 case 3:
		    substring(sayings);
		    break;

         // Save all sayings in new text file
		 case 4:
		    save(sayings);
		    break;

         // Allow user to delete saying
         case 5:
            remove(sayings);
            break;

         // Quit program
		 case 6:
		    doExit = true;

      } // End of switch
   } // End of while

   return 0;
} // End of main

// Function to display all sayings
void disp(list<string> sayings){
   int i = 1;
   for (auto it = sayings.begin(); it != sayings.end(); it++){
      cout << "--> " << i++ << ". " << *it << endl;
   }
}

// Function to add a new saying
void add(list<string> &sayings){
   // Get user input
   string userSaying;
   cout << "Please enter a new saying: ";
   getline(cin, userSaying);
   sayings.push_back(userSaying);
   sayings.sort();
}

// Function to display sayings with given substring
void substring(list<string> sayings){
   // Prompt user for substring
   cout << "Please enter a word or phrase: ";
   string phrase;
   getline(cin, phrase);
   // Display all strings containing substring
   bool sayingFound = false;
   for (auto it = sayings.begin(); it != sayings.end(); it++){
      string line = *it;
      if (line.find(phrase) != string::npos){
         cout << *it << endl;
         sayingFound = true;
      }
   }
   if (!sayingFound){
      cout << "No sayings contain that phrase." << endl;
   }
}

// Function to save sayings to text file
void save(list<string> sayings){
   // Prompt user for name of new data file
   cout << "Please enter output file name: ";
   string filename2;
   cin >> filename2;
   // Verify output connection
   ofstream ofs;
   ofs.open(filename2);
   if (!ofs){
      cout << "ERROR: Could not create new file.";
   }
   // Save sayings to new text file
   else{
      for (auto it = sayings.begin(); it != sayings.end(); it++){
         ofs << *it << endl;
      }
   }
}

// Function to remove saying from list
void remove(list<string> &sayings){
   // Prompt user to choose a saying and verify input
   disp(sayings);
   cout << endl;
   cout << "Choose a saying to delete: ";
   int choice;
   cin >> choice;
   while (choice < 1 || choice > sayings.size()){
      cout << "Please enter an integer between 1 and " << sayings.size() << ": ";
      cin >> choice;
   }
   // Delete saying
   auto it = sayings.begin();
   advance(it, choice - 1);
   sayings.erase(it);
}
