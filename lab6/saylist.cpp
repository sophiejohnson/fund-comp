// Sophie Johnson
// CSE 20311
// 10/7/17
// This program stores sayings. It begins by prompting the user for a
// startup data file. Then, it displays a menu of options for the user:
// display all sayings, add a new saying, list sayings containing a
// specific substring, save all sayings in a new text file, and quit
// the program.

// INCLUDES EXTRA CREDIT FEATURES

#include <iostream>
#include <fstream>
#include <list>
#include <string>
using namespace std;

// Declare functions
void sortSaying(list<string> &, string);
void disp(list<string>);
void add(list<string> &);
void substring(list<string>);
void save(list<string>);
void remove(list<string> &);
string strToLower(string);
bool isEmpty(string);

int main(){

   // Prompt user to enter startup data file or press enter
   cout << "Please enter a startup text file or press enter to begin: ";
   string filename;
   list<string> sayings;
   char c = cin.get();
   // Skip to rest of program if user presses enter
   if (c == '\n'){
   }
   // Record file name if user inputs one
   else{
      cin >> filename;
      filename = c + filename;

      // Verify connection to data file
      ifstream ifs;
      ifs.open(filename);
      if (!ifs){
         cout << "ERROR: File could not be found." << endl;
         return 1;
      }

      // Read sayings from startup data file into vector
      string newLine;
      getline(ifs, newLine);
      while (!ifs.eof()){
         sortSaying(sayings, newLine);
         getline(ifs, newLine);
      }
   }

   // While loop until exit condition met
   bool doExit = false;
   while (!doExit){

      // Display options
      cout << endl;
      cout << "1) Display all sayings" << endl;
      cout << "2) Enter a new saying" << endl;
      cout << "3) List all sayings that contain a given word" << endl;
      cout << "4) Save all sayings in new text file" << endl;
      cout << "5) Delete a saying" << endl;
      cout << "6) Quit the program" << endl;
      cout << "Please choose an option: ";

      // Check user input
      int userChoice = 0;
      cin >> userChoice;
      while (userChoice < 1 || userChoice > 6){
         cout << "Please enter an integer between 1 and 6: ";
         cin >> userChoice;
      }
      cout << endl;
      cin.ignore();

      // Perform action based on input
      switch (userChoice){

         // Display all sayings
		 case 1:
		    disp(sayings);
	        break;

         // Add new saying
		 case 2:
		    add(sayings);
		    break;

         // List sayings containing given substring
		 case 3:
		    substring(sayings);
		    break;

         // Save all sayings in new text file
		 case 4:
		    save(sayings);
		    break;

         // Allow user to delete saying
         case 5:
            remove(sayings);
            break;

         // Quit program
		 case 6:
		    doExit = true;

      } // End of switch
   } // End of while

   return 0;
} // End of main

// Function to insert saying in proper position of vector
void sortSaying(list<string> &sayings, string newSaying){
   // Do not add to vector if empty string
   if (isEmpty(newSaying)){
   }
   // Add first element
   else if (sayings.size() == 0){
      sayings.push_back(newSaying);
   }
   // Find position for other elements
   else{
      bool positionFound = false, goesAfter = false;
      auto it = sayings.begin();
      while ((!positionFound) && (it != sayings.end())){
         int i = 0;
         goesAfter = false;
         // Compare new saying to each saying in vector
         string compString, lowerNewSaying;
         compString = *it;
         compString = strToLower(compString);
         lowerNewSaying = strToLower(newSaying);
         // Check each letter for alphabetical order
         while ((!positionFound) && (!goesAfter) && (i != compString.length())){
            // Determine whether saying goes before
            if (lowerNewSaying[i] < compString[i]){
               positionFound = true;
            }
            // Determine whether saying goes after
            else if (lowerNewSaying[i] > compString[i]){
               goesAfter = true;
            }
            i++;
         }
         it++;
      }
      // If saying is last in alphabetical order, add to end
      if ((goesAfter) && (it == sayings.end())){
         sayings.push_back(newSaying);
      }
      // Otherwise, add saying before the saying identified above
      else{
         sayings.insert(--it,newSaying);
      }
   }
}

// Function to display all sayings
void disp(list<string> sayings){
   if (sayings.size() != 0){
      int i = 1;
      for (auto it = sayings.begin(); it != sayings.end(); it++){
         cout << "--> " << i++ << ". " << *it << endl;
      }
   }
}

// Function to add a new saying
void add(list<string> &sayings){
   // Get user input
   string userSaying;
   cout << "Please enter a new saying: ";
   // Ignore enter key
   char c = cin.get();
   while (isspace(c)){
      c = cin.get();
   }
   getline(cin, userSaying);
   userSaying = c + userSaying;
   // Add new saying to list
   sortSaying(sayings, userSaying);
}

// Function to display sayings with given substring
void substring(list<string> sayings){
   // Prompt user for substring
   cout << "Please enter a word or phrase: ";
   string phrase;
   getline(cin, phrase);
   // Change phrase to lower case
   phrase = strToLower(phrase);
   // Check all strings for substring
   bool sayingFound = false;
   for (auto it = sayings.begin(); it != sayings.end(); it++){
      string line = *it;
      // Change to lower case
      string lowerLine = strToLower(line);
      // Display all strings containing substring
      if (lowerLine.find(phrase) != string::npos){
         cout << "-->" << *it << endl;
         sayingFound = true;
      }
   }
   if (!sayingFound){
      cout << "No sayings contain that phrase." << endl;
   }
}

// Function to save sayings to text file
void save(list<string> sayings){
   // Prompt user for name of new data file
   cout << "Please enter output file name: ";
   string filename2;
   cin >> filename2;
   // Verify output connection
   ofstream ofs;
   ofs.open(filename2);
   if (!ofs){
      cout << "ERROR: Could not create new file.";
   }
   // Save sayings to new text file
   else{
      for (auto it = sayings.begin(); it != sayings.end(); it++){
         ofs << *it << endl;
      }
   }
}

// Function to remove saying from list
void remove(list<string> &sayings){
   // Output if no sayings in list
   if (sayings.size() == 0){
      cout << "There are no sayings to delete." << endl;
   }
   // Prompt user to choose which saying to delete
   else{
      disp(sayings);
      cout << endl;
      cout << "Choose a saying to delete: ";
      int choice;
      cin >> choice;
      while (choice < 1 || choice > sayings.size()){
         cout << "Please enter an integer between 1 and " << sayings.size() << ": ";
         cin >> choice;
      }
      // Delete saying
      auto it = sayings.begin();
      advance(it, choice - 1);
      sayings.erase(it);
   }
}

// Function to convert string to lowercase string
string strToLower(string phrase){
   for (int i = 0; i < phrase.length(); i++){
      if (isupper(phrase[i])){
         phrase[i] = tolower(phrase[i]);
      }
   }
   return phrase;
}

// Function to check whether string is blank or empty
bool isEmpty(string saying){
   bool stringIsEmpty = true;
   for (int i = 0; i < saying.length(); i++){
      if (!isspace(saying[i])){
         stringIsEmpty = false;
      }
   }
   return stringIsEmpty;
}
