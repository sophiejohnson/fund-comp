// Sophie Johnson
// CSE 20311
// 9/4/17
// Part 1: Multiplication Table
// This program asks the user to input two integers. Then, the program
// displays an integer multiplication table with the inputted value
// as dimensions.

#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

int main() {

   // Initialize table dimensions
   int rows;
   int cols;

   // Ask user for input
   cout << "This program creates an integer multiple table." << endl;
   cout << "Please enter the size of the table (rows, columns)." << endl;
   cin >> rows; 
   cin >> cols;

   // Find number of digits in largest product
   int largeProduct;
   largeProduct = rows * cols;
   int numDigits;
   numDigits = floor(log10(largeProduct)) + 1;
   
   int numSpaces;
   numSpaces = numDigits + 2;

   // Format first row
   cout << setw(numSpaces) <<  "*";
   for (int i = 1; i <= cols; i++) {
      cout << setw(numSpaces) <<  i;  
   }
   cout << endl;   

   // Format rest of table
   for (int i = 1; i <= rows; i++){
      // Format first column      
      cout << setw(numSpaces) << i;
      // Make multiplication table
      for (int j = 1; j <= cols; j++){
         int product = i * j;
         cout << setw(numSpaces) << product;
      }   
      cout << endl;
   }    
   return (0);
}
