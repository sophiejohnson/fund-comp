// Sophie Johnson
// CSE 20311
// 9/10/17
// Part 3: ASCII Art Graphs
// This program generates an ASCII graph of the mathmatical function:
// y = 4 log (x + 1) * sin(x) + 20
// It then states the function, range, maximum, and minimum.

#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

int main(){

   // Set number of x values to be evaluated
   int j;
   int jmax = 100;

   // Initialize min and max values at f(0)
   float minXValue = 0;
   float ymin = 4 * log2(minXValue + 1) * sin(minXValue) + 20;
   float maxXValue  = minXValue;
   float ymax = ymin;

   // Set range for x
   float xmin = 0;
   float xmax = 20;

   // Display equation
   cout << "Plot of y = 4 log(x + 1) * sin(x) + 20 from x = " << xmin << " to x = " << xmax << endl;
   cout << endl;

   // Create column headers
   cout << setw(6) << right << "X";
   cout << setw(6) << right << "Y" << endl;   

   // Display x value, y value, and appropriate number of symbols
   for (j = 0;(j <= jmax);(j++)){
      // Find x and y
      float x = ((xmin + xmax) / jmax) * j; 
      float y = 4 * log(x + 1) * sin(x) + 20;
      cout << fixed << setprecision(2);
      cout << setw(7) << right << x;
      cout << setw(7) << right << y << " ";
      // Create graph
      int numSymbols;
      numSymbols = ceil(y);
      int i;
      for(i = 1;(i <= numSymbols);i++){
        cout << "|";
      }
      cout << endl;
      // Check if y value is min or max
      if (y < ymin){
         ymin = y;
         minXValue = x;
      }
      else if (y > ymax){
         ymax = y;
         maxXValue = x;
      }
   }
   // Display maximum and minimum values
   cout << endl;
   cout << "The maximum value of " << ymax << " occurred at x = " << maxXValue << endl;
   cout << "The minimum value of " << ymin << " occurred at x = " << minXValue << endl;
   return(0);
}

