// Sophie Johnson
// CSE 20311
// 9/10/17
// Part 2: Mortgage Calculator
// This program will take user inputs for principle loan, interest rate, and
// desired monthly payment.  It will then output an amortization table.

#include <iostream>
#include <iomanip>
#include <cmath>
using namespace std;

int main() { 

   // Collect user inputs
   float balance;
   float yearlyInterestRate;
   float monthlyPayment;

   cout << "Please enter the principal, the yearly interest rate, and the desired monthly payment." << endl;
   cin >> balance;
   cin >> yearlyInterestRate;
   cin >> monthlyPayment; 

   // Find monthly interest rate
   float interestRate;
   interestRate = yearlyInterestRate / 12.;

   // Check user inputs

   bool validInputs = false;
   while (!validInputs){
      if (balance < 0){
         cout << "Please enter a positive principal: ";
         cin >> balance;
      }
     else if (yearlyInterestRate < 0){
         cout << "Please enter a positive yearly interest rate: ";
         cin >> yearlyInterestRate;
         interestRate = yearlyInterestRate / 12.;
      }
      else if (monthlyPayment < 0){
         cout << "Please enter a positive monthly payment: ";
         cin >> monthlyPayment;
      }
      else if (interestRate * balance > monthlyPayment){ 
         cout << "The desired payment is too small. Please enter a new payment: ";
         cin >> monthlyPayment;
      }
      else{
         validInputs = true; 
      }
   }

   // Adjust spacing based on input
   int numSpaces;
   numSpaces = floor(log10(balance)) + 10;

   // Display first row
   cout << setw(numSpaces - 5) << left << "Month";
   cout << setw(numSpaces) << right << "Payment";
   cout << setw(numSpaces) << right <<  "Interest";
   cout << setw(numSpaces) << right << "Balance" << endl;

   // Calculate and display monthly payments
   int month = 1;
   float totalPayment = 0;
   while (balance > 0){
     
      float interest;
      interest = interestRate * balance;

      // Check to see if balance is less than monthly payment
      if (interest + balance < monthlyPayment) {
         monthlyPayment = interest + balance;
      }

      balance = balance + interest - monthlyPayment;

      // Display results
      cout << setw(numSpaces - 5) << left << month;
      cout << fixed << setprecision(2);
      cout << "    $" << setw(numSpaces - 5) << right << monthlyPayment;
      cout << "    $" << setw(numSpaces - 5) << right << interest;
      cout << "    $" << setw(numSpaces - 5) << right << balance << endl;
      
      month++;
      totalPayment = totalPayment + monthlyPayment;
   }

   // Calculate total payments over time
   month = month--;
   int numYears;
   numYears = month / 12;
   int numMonths;
   numMonths = month % 12;
   cout << "You paid a total of $" << totalPayment << " over " << numYears;
   cout << " years and " << numMonths << " months." << endl;
   
   return(0);
}
